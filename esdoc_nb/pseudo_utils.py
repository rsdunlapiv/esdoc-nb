#
# Utility methods; may not call anything but library imports
# (to avoid circular imports)
#
import gtk

def ibutton(stock_item,label, tooltip = None):
    """ Create and return a stock image button """
    image = gtk.Image()
    image.set_from_stock(stock_item,gtk.ICON_SIZE_BUTTON)
    button = gtk.Button()
    button.set_image(image)
    button.set_label(label)

    if tooltip:
        tipper = gtk.Tooltips()
        tipper.set_tip(button, tooltip)
    return button

