__author__ = 'BNL28'

import unittest
import shutil
import os
import tempfile

import gtk

import pseudo_utils as pu
import esdoc_nb.mp.pseudo_mp as mp
import esdoc_nb.mp.cmip6 as cmip6
import pseudo_storage as ps
from pseudo_registry import Registry


class baseEntry(gtk.HBox):
    """ Simple entry box """
    def __init__(self, p, callback):
        """
        Entry box which clears when callback is activated.
        :param p: a uml property
        :param callback: to be called when entry changes
        :return: a gtk.Entry widget in a gtk.HBox
        """
        super(baseEntry, self).__init__(spacing=5)
        self.label = gtk.Label(p.name)
        tooltip = gtk.Tooltips()
        tooltip.set_tip(self.label, p.doc)

        self.pack_start(self.label, False, False, 5)
        self.callback = callback
        self.textEntry = gtk.Entry()
        self.textEntry.connect("activate", self.callback)

        self.pack_start(self.textEntry, True, True, 5)

    def set(self, value):
        """Set value of text entry """
        if value != None:
            self.textEntry.set_text(value)


class docHolder(gtk.Frame):
    """ Holds an entry for the selected document in the case of a single document selection"""

    def __init__(self, title, xsize=200, ysize=50):

        super(docHolder, self).__init__(title)
        self.set_size_request(xsize,ysize)

        self.label = gtk.Label()

        self.add(self.label)

        self.uid = None

        self.show_all()

        self.doc = None

    def set_content(self, doc):
        self.label.set_text(doc.name)
        self.uid = doc.meta.uid
        self.doc = doc

class docSelector(gtk.Frame):
    """ Provides a widget for document selection """

    def __init__(self, title, selection_callback, select = True, xsize=200, ysize=250):
        """Initialise as an empty vessel which gets populated
        via the set_data method. Needs a selection callback for when
        the selection is changed. """

        super(docSelector, self).__init__(title)
        self.set_size_request(xsize, ysize)
        self.xsize=xsize

        self.selection_callback = selection_callback

        self.vbox = gtk.VBox()

        # instruction
        if select:
            ins = gtk.Label('<small>Click on rows to select (use CNTRL-F to search) </small>')
        else:
            ins = gtk.Label('<small>Click on row to remove selection</small>')
        ins.set_use_markup(True)
        self.select = select
        self.vbox.pack_start(ins, False, True, 5)

        # use a scrolled window to hold a list store for examining variables
        self.sw = gtk.ScrolledWindow()
        self.sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        # create a tree view list store
        self.view = gtk.TreeView(None)
        self.view.set_search_column(0)          # search on document names
        self.view.set_rules_hint(True)          # nice alternating lines
        self.view.set_headers_clickable(True)   # can reorder on column headers

        # now set a liststore for the treeview.
        # [document name, validity, uid
        # (But we won't show column 3)
        self.listStore = gtk.ListStore(str, str, str)

        # bind the store to the tree view
        self.view.set_model(self.listStore)

        #The cell renderer is used to display the text in list store.
        self.fieldRenderer = gtk.CellRendererText()
        for k, v in (('xpad', 10), ('size-points', 8)):
            self.fieldRenderer.set_property(k, v)

        self.initialising = True
        self.columns_are_setup = False

        # Add the tree view to the scrolled window and the sw to self (frame)
        self.sw.add(self.view)
        self.vbox.pack_start(self.sw, True, True, 2)
        self.add(self.vbox)

    def _setColumns(self):
        """ Sets the base_columns. We do this as late as possible, so
        the widget knows how big it is and get can the sizing right."""

        # work out how big we are so we can get the right column sizes
        xsize = self.xsize

        # column headings
        headings = ['Document Name', 'Validity']
        colsizes = [int(xsize*0.8), int(xsize*0.2)]

        i = 0
        for h in headings:

            col = gtk.TreeViewColumn(h, self.fieldRenderer, text=i)
            col.set_sort_column_id(i)    # is sortable
            #col.set_alignment(0.5) # centred data

            # Each column is fixed width, dependant on screen size
            col.set_property('sizing', gtk.TREE_VIEW_COLUMN_FIXED)
            col.set_fixed_width(colsizes[i])

            # Add the column created to the tree view
            self.view.append_column(col)

            i += 1

        # Setup the selection, but make sure we only call the
        # select function when we have actually selected something
        # not when we are setting up, or have just removed something.

        self.fieldChoice = self.view.get_selection()
        self.fieldChoice.set_select_function(self._row_selection)
        self.columns_are_setup = True

    def _add_doc2store(self, document):
        """Given a document, add facets to the list store"""
        entry = document.name, \
                {1:'',0:'invalid'}[document.validate(True)],\
                document.meta.uid
        treeiter = self.listStore.append(entry)
        return treeiter

    def set_selector_data(self, registry):
        """ Loop over contents of registry names list (which consists of (name,uid) tuples """

        if not self.columns_are_setup: self._setColumns()
        self.initialising = True
        self.select_toggle = True

        # clear existing content, if any
        if len(self.listStore) != 0:
            self.listStore.clear()

        # loop over fields
        for field in registry.names:
            doc = registry.get(field[1])
            treeiter = self._add_doc2store(doc)

        self.initialising = False

    def _row_selection(self, rowtuple):
        ''' Called when a row in the liststore is selected '''
        # at this point we have a tuple self.select_toggle = True that looks like
        # (7,)
        print 'Row Selection!', self.select_toggle, self.initialising
        if self.initialising: return
        treeiter = self.listStore.get_iter(rowtuple)
        name, validity, uid = tuple(self.listStore[treeiter])
        # For some reason this gets called twice when
        # I remove a row, so we need to ignore the second call.
        if self.select_toggle:
            self.listStore.remove(treeiter)
            self.selection_callback(self, uid)
        self.select_toggle = not self.select_toggle

    def show(self):
        ''' Show widgets '''
        print 'hello'
        super(docSelector, self).show_all()

    def make_active(self):
        """Can't let the row selection be called until we are ready"""
        self.initialising = False

class cimlinkDialog(object):

    """ Provides a special dialog for making a cimlink.
    This is a special case of a cim document dialog, because we want to allow folks
    to choose from existing documents to link to, or, create a link to an (as yet)
    non-existing document, or, in the fullness of time, to interrogate remote repositories
    for choices.
    """

    def __init__(self, theProperty, parent_callback, idstring, store):

        """
        Constructs a cimlink dialog window.
        :param theProperty: a umlProperty instance which conforms to the linked_to stereotype
        :param parent_callback: the callback for changes to properties.
        :param idstring: title of window
        :param store: the document repository interface
        :return: A dialog window which runs until terminated.
        """

        self.parent_callback = parent_callback
        self.store = store
        self.target = theProperty.target
        self.name = theProperty.name
        self.multi = not theProperty.single_valued

        # a dialog is essentially a vbox
        self.dialog = gtk.Dialog(title=idstring)
        self.dialog.set_border_width(5)

        # five parts to the dialog
        # documentation part
        # selected list
        # selection list part
        # new entry
        # buttons

        # Part One: Documentation

        documentation = """
        <big> %s: %s </big>\n\n (select %s)
        """ % (self.name, self.target, theProperty.cardinality)

        header = gtk.Label(documentation)
        header.set_use_markup(True)
        header.set_alignment(0.5,0)
        header.set_justify(gtk.JUSTIFY_CENTER)
        self.dialog.vbox.pack_start(header, False, True, 2)

        line1 = gtk.HSeparator()
        self.dialog.vbox.pack_start(line1, False, True, 2)

        # Part Two: Selected

        def fromLink(link):
            """ Local method to return document from a cimlink """
            # FIXME Since this was written we have added a new method to pseudo_storage
            # get_local_doc_by_link ... some of this method could use that.
            # look out for issues around whether we want the doc or copy of it.
            assert link.protocol.startswith('local'), "Protocol %s not expected " % link.protocol
            assert link.linkage.startswith('nb://'),"Linkage %s not expected " % link.linkage
            try:
                doc = self.store.get(link.linkage[5:], self.target)
            except KeyError,e:
                # There is a possibility the cimtype of this document
                # is incorrect, since it may be an alternative and have been changed
                # since it was set.
                # FIXME Doing this may have consequences upstream we have to worry about.
                # FIXME This issue is reported in ticket 136.
                mock = mp.make_cim_instance(self.target)
                if mock.alternatives:
                    # Attempt to get this document from the other registries
                    for r in mock.alternatives:
                        try:
                            doc = self.store.get(link.linkage[5:], r)
                            return doc
                        except KeyError:
                            pass
                raise KeyError,e
            return doc

        if not self.multi:
            self.selectedOne = docHolder('Current Selection')
            if theProperty.value:
                self.selectedOne.set_content(fromLink(theProperty.value))
            self.dialog.vbox.pack_start(self.selectedOne, False, True, 2)
        else:
            self.selectedRegistry = Registry(self.target)
            for link in theProperty.value:
                self.selectedRegistry.add(fromLink(link))
            self.selected = docSelector('Current Selection', self._removal_made, select=False, ysize=150)
            self.selected.set_selector_data(self.selectedRegistry)
            self.dialog.vbox.pack_start(self.selected, True, True, 2)

        line2 = gtk.HSeparator()
        self.dialog.vbox.pack_start(line2, False, True, 2)

        # Part Three: Chooser on existing documents
        # Need a copy of the registry without documents already selected
        self.selectorRegistry = Registry(self.target)
        #
        # Need to consider sub-class alternatives as well.
        mock = mp.make_cim_instance(self.target,self.store.factory)
        set_of_registries = [self.target,] + mock.alternatives
        for r in set_of_registries:
            if r in self.store.registries:
                for d in self.store.registries[r]:
                    doc = self.store.get(d, r)
                    if self.multi:
                        if doc.meta.uid not in self.selectedRegistry:
                            self.selectorRegistry.add(doc)
                    elif doc.meta.uid != self.selectedOne.uid:
                        self.selectorRegistry.add(doc)

        self.selector = docSelector('Document Selector', self._selection_made)
        self.selector.set_selector_data(self.selectorRegistry)
        self.dialog.vbox.pack_start(self.selector, True, True, 2)

        line3 = gtk.HSeparator()
        self.dialog.vbox.pack_start(line3, False, True, 2)

        # Part Four: New entry

        p = mp.umlProperty(('New %s' % theProperty.target,'str','1.1',
                'Placeholder for a new %s ' % theProperty.target))
        self.single = baseEntry(p, self._newdocument)
        self.dialog.vbox.pack_start(self.single, False, True, 2)

        line4 = gtk.HSeparator()
        self.dialog.vbox.pack_start(line4, False, True, 2)

        # Part Five: Buttons
        bspace = 4
        bbox = gtk.HBox(spacing=bspace)
        self.dialog.vbox.pack_start(bbox, expand=False, padding=bspace)

        b = pu.ibutton(gtk.STOCK_SAVE,'','Save %s' % self.name)
        bbox.pack_start(b, expand=False, padding=bspace)
        self.saver = b
        self.saver.connect('clicked', self._makelinks)

        b = pu.ibutton(gtk.STOCK_HELP, '')
        bbox.pack_start(b, expand=False, padding=bspace)
        self.helper = b
        self.helper.connect('clicked',self._helper)

        b = pu.ibutton(gtk.STOCK_CLEAR,  '', 'Reset ')
        bbox.pack_start(b, expand=False, padding=bspace)
        self.breset = b
        self.breset.connect('clicked',self.reset)

        b = pu.ibutton(gtk.STOCK_QUIT, '')
        bbox.pack_start(b, expand=False, padding=bspace)
        self.closer = b
        self.closer.connect('clicked',self.destroy)

        # For reasons I don't understand the show_all statement forces the selected
        # docSelector to run the row_selection, but not the selector-docSelector.
        # and we don't want that to happen at initialisation, hence the
        # make active calls and toggling. This is a hack ... I should probably
        # add a connect button to run after the show to actually make the connections.

        if self.multi: self.selected.initialising = True
        self.dialog.show_all()
        if self.multi:
            self.selected.select_toggle = not self.selected.select_toggle
            self.selected.make_active()
        self.selector.make_active()

    def _newdocument(self, w):
        """Callback from base entry to create new document"""
        nd = self.store.factory.build(self.target)
        nd.meta = self.store.makeDefaultMeta()
        nd.name = w.get_text()
        print 'New document instance made %s for %s has uid %s ' % (
            nd, self.target, nd.meta.uid)
        self.store.add(nd)
        if self.multi:
            self.selectedRegistry.add(nd)
            self.selected.set_selector_data(self.selectedRegistry)
        else:
            if self.selectedOne.uid:
                self.selectorRegistry.add(self.selectedOne.doc)
                self.selector.set_selector_data(self.selectorRegistry)
                self.selector.select_toggle = not self.selector.select_toggle
            self.selectedOne.set_content(nd)
        self.single.set(' ')

    def _selection_made(self,w, uid):
        """
        Callback from the selector
        :param w: The widget from which this call back originated
        :param uid: The uid of the document to be added to the selection
        :return:
        """
        doc = self.selectorRegistry.get(uid)
        self.selectorRegistry.delete(uid)
        print 'selection made ', doc.name
        if self.multi:
            self.selectedRegistry.add(doc)
            self.selected.set_selector_data(self.selectedRegistry)
        else:
            if self.selectedOne.uid:
                self.selectorRegistry.add(self.selectedOne.doc)
                self.selector.set_selector_data(self.selectorRegistry)
                self.selector.select_toggle = not self.selector.select_toggle
            self.selectedOne.set_content(doc)

    def _removal_made(self, w , uid):
        """
        Callback from the selected docSelector to say something has been removed
        :param w: The widget from which this call back originated
        :param uid: The uid of the document to be added to the selection
        :return:
        """
        doc = self.store.get(uid, self.target)
        self.selectedRegistry.delete(uid)
        print 'removal made', doc.name
        # put it back in the selector docSelector
        self.selectorRegistry.add(doc)
        self.selector.set_selector_data(self.selectorRegistry)

    def _makelinks(self, w):
        """
        When save button is clicked, create the cimlinks, and
        pass them back to the parent callback
        :param w: The button widget from which this callback originated
        :return None
        """
        if self.multi:
            results = []
            for uid in self.selectedRegistry:
                doc = self.selectedRegistry.get(uid)
                results.append(self.store.link_to_doc(doc))
        else:
            doc = self.selectedOne.doc
            results = self.store.link_to_doc(doc)

        # I think the parent needs to kill this widget
        self.parent_callback(self, results)

    def reset(self):
        raise NotImplementedError

    def _helper(self):
        raise NotImplementedError

    def run(self):
        return self.dialog.run()

    def destroy(self,w=None):
        self.dialog.destroy()

class AlternativeTest(unittest.TestCase):
    """ test use of alternatives"""
    def setUp(self):
        self.tdir = tempfile.mkdtemp()
        self.store = ps.cimStorage(self.tdir)
        author = mp.make_cim_instance('Party')
        author.name = 'LinkTest Unittest'
        self.store.set_default_author(author)
        self.uids = []
        self.factory=mp.cimFactory()
        i = 0
        egs = ['NumericalRequirement', 'NumericalRequirement', 'ForcingConstraint', 'ForcingConstraint']
        for d in egs:
            doc = self.factory.build(d)
            doc.name = 'Test %s %s' % (d,i)
            i+=1
            meta = self.store.makeDefaultMeta()
            doc.meta = meta
            self.uids.append(meta.uid)
            self.store.add(doc)
        self.prop = mp.umlProperty(('requirements','linked_to(NumericalRequirement)','1.N','dummy'))
        self.sprop = mp.umlProperty(('requirement','linked_to(NumericalRequirement)','1.1','dummy'))

    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone, False)

    def testAlternateDialog(self):
        """Test a dialog with alternatives"""

        def callback(w,vv):
            for v in vv:
                print v, v.cimType, v.remote_type
            w.destroy()

        x = cimlinkDialog(self.prop,callback,'testing alternative dialog',self.store)
        x.run()

    def testAlternativeSingleDialog(self):

        def callback(w,v):
            print v, v.cimType, v.remote_type
            w.destroy()

        x = cimlinkDialog(self.sprop, callback, 'testing alternative dialog', self.store)
        x.run()

class LinkTest(unittest.TestCase):

    def setUp(self):
        self.tdir = tempfile.mkdtemp()
        self.store = ps.cimStorage(self.tdir)
        author = mp.make_cim_instance('Party')
        author.name = 'LinkTest Unittest'
        self.store.set_default_author(author)
        self.uids=[]
        for i in range(5):
            doc = mp.make_cim_instance('Project')
            doc.name = 'Test Project %s' % i
            meta = mp.make_cim_instance('Meta')
            doc.meta = meta
            self.uids.append(meta.uid)
            if i> 3:
                doc.description = mp.makeQuickText('abc')
            self.store.add(doc)
        self.prop = mp.umlProperty(('sub_project','linked_to(Project)','1.N','dummy'))
        self.sprop = mp.umlProperty(('sub_project','linked_to(Project)','1.1','dummy'))

    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone, False)

    def testSelector(self):
        """See if we can construct an empty selector """

        def callback(w,v):
            print v
        x = docSelector('Document Selector', callback)

        self.assertTrue(isinstance(x,docSelector))

    def testEmptyDialog(self):
        """ Test an empty property"""

        def callback(w,vv):
            print 'Empty test results ',[v.name for v in vv]
            w.destroy()

        x = cimlinkDialog(self.prop,callback,'testing empty dialog',self.store)
        x.run()

    def testInitDialog(self):
        """Test an initialised dialog"""

        docs = [self.store.get(uid, 'Project') for uid in self.uids[2:4]]
        initvalue = [self.store.link_to_doc(doc) for doc in docs]
        print initvalue
        self.prop.set(initvalue)

        def callback(w,vv):
            print [v.name for v in vv]
            w.destroy()

        x = cimlinkDialog(self.prop,callback,'testing initialised dialog',self.store)
        self.assertEqual(len(x.selectedRegistry), len(initvalue))
        x.run()

    def testSingleDialog(self):

        def callback(w,vv):
            print vv.name
            w.destroy()

        x = cimlinkDialog(self.sprop,callback,'Testing single valued dialog',self.store)
        x.run()

class ComplicatedTests(unittest.TestCase):

    def setUp(self):
        self.tdir = tempfile.mkdtemp()
        self.store = ps.cimStorage(self.tdir)
        author = mp.make_cim_instance('Party')
        author.name = 'LinkTest Unittest'
        self.store.set_default_author(author)
        self.store.extend(cmip6.get_constructors())

    def testExtensionDialog(self):
        """ Test dealing with an extension dialog """
        def callback(w,vv):
            print vv.name
            w.destroy()

        x = mp.make_cim_instance('Model',self.store.factory)
        self.assertEqual('Cmip6Model',x.cimType)

        sea_ice = x.attributes['sea_ice_domain_description']

        x = cimlinkDialog(sea_ice,callback,'Testing sea ice',self.store)
        x.run()