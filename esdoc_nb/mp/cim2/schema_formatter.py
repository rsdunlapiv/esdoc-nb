"""
.. module:: esdoc_nb.mp.cim2.schema_reformatter.py
   :copyright: Copyright "Feb 7, 2013", Earth System Documentation
   :license: GPL/CeCIL
   :platform: Unix, Windows
   :synopsis: Reformats schema definitions so as to confirm to esdoc-mp.

.. moduleauthor:: Mark Conway-Greenslade <momipsl@ipsl.jussieu.fr>


"""

# Map of package names to reformatted package names.
_PACKAGE_REFORMAT_MAP = {
    "shared_time": 'shared'
}

# Map of reformtted base class names.
_BASE_CLASS_REFORMAT_MAP = {
    "designing.activity": 'activity.activity'
}

# Set of class types to be excluded.
CLASS_BLACKLIST = {
    'shared.meta',
    'shared.minimal_meta'
}


def reformat_class(package, cls):
    """Reformats a class definition to conform to esdoc-mp.

    """
    def _get_base():
        """Gets reformatted base class reference.

        """
        base = cls.get('base', None)

        # Ensure base class references are lower case.
        if base:
            base = base.lower()

        # Ensure that base class references are prefixed with package name.
        if base and len(base.split('.')) != 2:
            base = "{0}.{1}".format(package, base)

        # Remap where appropriate.
        try:
            base = _BASE_CLASS_REFORMAT_MAP[base]
        except KeyError:
            pass

        return base


    def _get_property_type(prop_name, prop_type):
        """Reformats a property type definition.

        """
        # Override document meta attributes.
        if prop_name == "meta":
            return "shared.doc_meta_info"

        # Ensure property types are lower case.
        prop_type = prop_type.lower()

        # Ensure linked_to references are stripped out.
        if prop_type.startswith('linked_to'):
            prop_type = prop_type[10:-1]
            if prop_type.find(".") == -1:
                prop_type = "{}.{}".format(package, prop_type)

        # Override text type.
        if prop_type == 'text':
            prop_type = 'str'

        # Override text type.
        if prop_type == 'shared.cimtext':
            prop_type = 'str'

        # Override complex type reference.
        if len(prop_type.split(".")) == 2:
            pkg, cls = prop_type.split(".")
            try:
                pkg = _PACKAGE_REFORMAT_MAP[pkg]
            except KeyError:
                pass
            else:
                prop_type = ".".join([pkg, cls])

        return prop_type


    def _get_property(prop):
        """Reformats a property definition."""
        # Strip out documentation strings.
        prop = prop[0:3]

        # Reformat property type definitions.
        return (prop[0], _get_property_type(prop[0], prop[1]), prop[2])


    def _get_reference_properties():
        """Returns so called reference properties.

        """
        result = []
        for prop in cls.get('properties', []):
            prop_name = prop[0]
            prop_type = prop[1].lower()
            prop_card = prop[2]
            if prop_type.startswith('linked_to'):
                if prop_card.endswith("N"):
                    result.append(("{}_references".format(prop_name), "shared.doc_reference", "0.N"))
                else:
                    result.append(("{}_reference".format(prop_name), "shared.doc_reference", "0.1"))

        return result


    def _get_properties():
        """Reformats property definitions.

        """
        result = [_get_property(p) for p in cls.get('properties', [])]
        result += _get_reference_properties()

        return result


    def _get_doc_strings():
        """Reformats property doc string."""
        result = cls.get('doc_strings', dict())
        for prop in cls.get('properties', []):
            if len(prop) == 4 and prop[0] not in result:
                result[prop[0]] = prop[3]

        return result


    return {
        'base': _get_base(),
        'doc_strings': _get_doc_strings(),
        'is_abstract': cls.get('is_abstract', False),
        'properties': _get_properties(),
        'type': 'class'
    }


def reformat_enum(package, enum):
    """Reformats an enum definition to conform to esdoc-mp.

    """
    return {
        'is_open': enum.get('is_open', False),
        'members': enum.get('members', []),
        'type': 'enum'
    }
