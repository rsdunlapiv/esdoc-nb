__author__ = 'BNL28'

#
# So the logic of these classes is to provide an infrastructure that allows
# us to build descriptions which have addressed specific questions of
# interest to the scientific community.
#
# We imagine that each "realm" level component is described by a set of
# process descriptions, each of which follows a structure which provides
# both a standardised description in terms of UML structure, and a
# "free text" description which has been constructed as a combination of
# provider free text informed by specific guidance from the
# community as to what they want to see in that free text.
#
# The order of the classes in this file represents a top-down hierarchical
# order of things one ought to be thinking about followed by the various
# necessary utility methods.
#

def model():
    """ A model component: can be executed standalone, and has as scientific
    description available via a link to a science.domain document. (A configured model can
     be understood in terms of a simulation, a model, and a configuration.)
    """
    return {
        'type': 'class',
        'base': 'software.component_base',
        'is_abstract': False,
        'properties': [
            ('category','science.model_types','1.1','Generic type for this model'),
            ('scientific_domain','linked_to(science.scientific_domain)', '0.N',
                "The scientific domains which this model simulates"),
            ('coupled_software_components','linked_to(science.model)','0.N',
                "Software components which are linked together using a coupler to deliver this model"),
            ('internal_software_components','software.software_component','0.N',
                'Software modules which together provide the functionality for this model'),
            ('coupler', 'software.coupling_framework', '0.1',
                'Overarching coupling framework for model'),
            ('extra_conservation_properties','science.conservation_properties','0.1',
                'Details of any extra methodology needed to conserve variables between coupled components'),
            ('meta', 'shared.meta', '1.1',
                'Metadata about how the model description was constructed'),
            ],
    }


def scientific_domain():
    """Scientific area of a numerical model - usually a sub-model or component.
    Can also be known as a realm"""
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('name', 'str', '1.1',
                'Name of the scientific domain (e.g. ocean)'),
            ('realm', 'str', '0.1',
                'Canonical name for the domain of this scientific area'),
            ('overview', 'shared.cimtext', '0.1',
                'Free text overview description of key properties of domain'),
            ('references', 'shared.reference', '0.N',
                "Any relevant references describing the implementation of this domain in a relevant model."),
            # Note that we can have the same grid using many different resolutions
            # so we pulled it up and out of the grid description.
            ('resolution', 'science.resolution', '1.1',
                'Default resolution of component'),
            ('simulates','science.process', '1.N',
                'Processes simulated within the domain'),
            ('extra_conservation_properties','science.conservation_properties','0.1',
                'Details of any extra methodology needed to conserve variables between processes'),
            ('grid','science.grid_summary','0.1',
                'Summary description of the grid upon which computations were carried out'),
            ('time_step', 'float', '1.1',
                'Timestep (in seconds) of overall component'),
            ('tuning_applied','science.tuning','0.1',
                'Describe any tuning used to optimise the parameters in this model/component'),
            ('meta', 'shared.meta', '1.1',
                'Metadata describing the construction of this domain description'),

        ]
    }

def grid_summary():
    """ Key scientific characteristics of the grid use to simulate a specific domain"""
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('grid_type', 'science.grid_types', '1.1',
                'Description of basic grid (e.g. "cubed-sphere")'),
            ('grid_layout', 'science.grid_layouts', '1.1',
                'Type of horizontal grid-layout (e.g. Arakawa C-Grid'),
            ('grid_extent','science.extent','1.1',
                'The extent of the computational domain in horizontal and vertical space'),
        ]
    }

def extent():
    """ Key scientific characteristics of the grid use to simulate a specific domain.
    Note that the extent does not itself describe a grid, so, for example, a polar
    stereographic grid may have an extent of northern boundary 90N, southern boundary
    45N, but the fact that it is PS comes from the grid_type."""
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'pstr': ('%s',('region_known_as',)),
        'properties': [
            ('region_known_as','str','0.N',
                'Identifier or identifiers for the region covered by the extent'),
            ('minimum_vertical_level','float','0.1',
                'Minimum vertical level'),
            ('maximum_vertical_level','float','0.1',
                'Maximum vertical level'),
            ('z_units','str','1.1',
                'Units of vertical measure'),
            ('is_global','bool','1.1',
                'True if horizontal coverage is global'),
            ('western_boundary','float','0.1',
                'If not global, western boundary in degrees of longitude'),
            ('eastern_boundary','float','0.1',
                'If not global, eastern boundary in degrees of longitude'),
            ('northern_boundary','float','0.1',
                'If not global, northern boundary in degrees of latitude'),
            ('southern_boundary','float','0.1',
                'If not global, southern boundary in degrees of latitude'),

        ]
    }


def process():
    """ Provides structure for description of a process simulated within a particular
    area (or domain/realm/component) of a model. This will often be subclassed
    within a specific implementation so that constraints can be used to ensure
    that the vocabulary used is consistent with project requirements."""
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('name', 'str', '1.1',
                'Short name for the process of interest'),
            ('description', 'str', '0.1',
                'Short description of the process which is being simulated.'),
            ('keywords', 'str', '1.1',
                'keywords to help re-use and discovery of this information.'),
            ('implementation_overview', 'shared.cimtext', '1.1',
                'General overview description of the implementation of this process.'),
            ('algorithm_properties', 'science.algorithm', '0.N',
                'Descriptions of algorithms and their properties used in the process'),
            ('detailed_properties','science.process_detail','0.N',
                'Sets of properties for this process.'),
            ('time_step_in_process', 'float','0.1',
                'Timestep (in seconds). Only needed if differing from parent component.'),
            ('references', 'shared.reference', '0.N',
                "Any relevant references describing this process and/or it's implementation"),
            ],
    }


def process_detail():
    """ Three possible implementations of process_detail:
        1) A generic description of some aspect of detail,
        2) Several specific descriptions selected from a vocabulary, or
        3) One specfic property selected from a vocabulary
        """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('heading', 'str', '0.1',
                'A heading for this detail description'),
            ('content', 'shared.cimtext', '0.1',
                'Free text description of process detail (if required).'),
            ('vocabulary', 'str', '0.1',
                'Name of an enumeration vocabulary of relevant sub-processes.'),
            ('selection', 'str', '0.N',
                'List of choices from the vocabulary of appropriate sub-processes.'),
            ('properties', 'shared.key_float', '0.N','Any relevant numeric properties'),
        ]
    }


def algorithm():
    """ Used to hold information associated with an algorithm which implements some key
    part of a process, and its properties. In most cases this is quite a high level concept
    and isn't intended to describe the gory detail of how the code implements the algorithm
    rather the key scientific aspects of the algorithm.
    """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('heading', 'str', '1.1',
                'Title for this collection of algorithm/property information.'),
            ('implementation_overview', 'shared.cimtext', '1.1',
                'Overview of the algorithm implementation'),
            ('references', 'shared.citation', '0.N',
                'Relevant references'),
            ('prognostic_variables', 'data.variable_collection', '0.N',
                'Prognostic variables associated with this algorithm'),
            ('diagnostic_variables', 'data.variable_collection', '0.N',
                'Diagnostic variables associated with this algorithm'),
            ('detailed_properties','science.process_detail','0.N',
                'Sets of properties for this algorithm.'),
        ],
    }

def resolution():
    """ Describes the computational spatial resolution of a component or process. Not intended
        to replace or replicate the output grid description.  When it appears as part of a process
        description, provide only properties that differ from parent domain. Note that where
        different variables have different grids, differences in resolution can be captured by
        repeating the number_of_ properties.
    """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('name', 'str', '1.1',
                'This is a string usually used by the modelling group to describe their model component ' +
                ' or process resolution,  e.g. N512L180 or T512L70 etc'),
            ('equivalent_horizontal_resolution', 'float', '1.1',
                'Resolution in metres of "typical grid cell" (for gross comparisons of resolution), eg. 50000 (50km)'),
            ('number_of_xy_gridpoints', 'int', '0.N',
                'Total number of horizontal points on computational grids'),
            ('number_of_levels', 'int', '0.N',
                'Number of vertical levels resolved'),
            ('is_adaptive_grid', 'bool', '0.1',
                'Default is False, set true if grid resolution changes during execution.'),
        ]
    }

def conservation_properties():
    """ Describes how prognostic variables are conserved """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('corrected_conserved_prognostic_variables','data.variable_collection', '0.1',
                'Set of variables which are conserved by *more* than the numerical scheme alone'),
            ('correction_methodology','shared.cimtext','0.1','Description of method by which correction was achieved'),
            ('flux_correction_was_used','bool','1.1','Flag to indicate if correction involved flux correction'),
        ]
    }

def tuning():
    """ Method used to optimise equation parameters in model component (aka "tuning") """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('description','shared.cimtext','1.1',
                'Brief description of tuning methodology. Include information about observational period(s) used'),
            ('global_mean_metrics_used','data.variable_collection','0.1',
                'Set of metrics of the global mean state used in tuning model parameters'),
            ('trend_metrics_used','data.variable_collection','0.1',
                'Which observed trend metrics have been used in tuning model parameters'),
            ('regional_metrics_used','data.variable_collection','0.1',
                'Which regional metrics of mean state (e.g Monsoons, tropical means etc) have been used in tuning.'),
        ]
    }
