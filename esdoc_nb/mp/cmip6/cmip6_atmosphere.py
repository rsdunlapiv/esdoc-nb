__author__ = 'CLP73'
version = '0.0.1'

#
# This is the candidate standard vocabulary for CMIP6 atmosphere.

# Note that the classes and enums need to be mixed to make it possible to easily develop these
# ab initio in python. If we develop a different toolchain, we can split them.

# Note that vocab_status attribute is used for two reasons: the primary reason is to
# help the test system known what needs to be done in vocabulary development. Classes
# in the CMIP6 package without the attribute, or at the wrong level, are deemed to be
# incomplete. It is also used in the notebook meta system to indicate that classes
# carrying this attribute will not be abstract, and will not carry properties.

#
# Domain definition
#


def _atmos_domain():
    """ Characteristics of the atmosphere simulation"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.scientific_domain',
        'constraints': [
            ('realm', 'value', 'Atmosphere'),
            ('simulates', 'include', ['cmip6.atmos_properties',
                                      'cmip6.atmos_dynamical_core',
                                      'cmip6.atmos_radiation',
                                      'cmip6.atmos_convective_turbulence_clouds',
                                      'cmip6.atmos_gravity_waves']),
        ]
    }

#
# Processes follow
#


def _atmos_properties():
    """ This is the catch all generic atmosphere description """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere'),
            ('detailed_properties', 'include', ['cmip6.atmos_model_family',
                                                'cmip6.atmos_basic_approximations',
                                                'cmip6.atmos_volcanoes_implementation',
                                                'cmip6.atmos_orography',
                                                'cmip6.atmos_top_of_atmosphere_insolation']),
        ]
    }


def _atmos_dynamical_core():
    """ Characteristics of the atmosphere dynamical core processes"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Dynamical Core'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_dynamical_core_attributes',
                'cmip6.atmos_timestepping_framework',
                'cmip6.atmos_horizontal_discretisation',
                'cmip6.atmos_horizontal_diffusion',
                'cmip6.atmos_advection_tracers',
                'cmip6.atmos_advection_momentum']),
            ]
     }


def _atmosphere_radiation():
    """ Characteristics of the atmosphere radiation process"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Radiation'),
            ('detailed_properties', 'include', [
                'cmip6.atmos_radiation_attributes',
                'cmip6.atmos_longwave',
                'cmip6.atmos_shortwave',
                'cmip6.atmos_cloud_radiative_properties']),
            ]
        }


def _atmos_turbulance_convection():
    """ Characteristics of the atmospheric turbulence and convection"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Convective Turbulence and Clouds'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_boundary_layer_turbulence',                
                'cmip6.atmos_deep_convection',
                'cmip6.atmos_shallow_convection',
                'cmip6.atmos_other_convection', ]),
            ('detailed_properties', 'include', [
                'cmip6.atmos_deep_convection_processes',
                'cmip6.atmos_shallow_convection_processes', ]),
            ]
        }


# Process: Atmosphere: microphysics and precipitation
def _atmos_microphysics_precipitation():
    """Characteristics of cloud microphysics and precipitation"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Cloud Microphysics and Precipitation'),
            ('detailed_properties', 'include', [
                'cmip6.atmos_large_scale_precipitation_scheme',
                'cmip6.atmos_large_scale_precipitation_hydrometeors',
                'cmip6.atmos_cloud_microphysics_scheme',
                'cmip6.atmos_cloud_microphysics_processes', ]),
            ]
        }


# Process Detail: Atmosphere: microphysics and precipitation: large scale precipitation scheme
def _atmos_large_scale_precipitation_scheme():
    """Commonly used name of the large scale precipitation parameterisation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('???', 'cmip6.atmos_large_scale_precipitation_scheme', '1.1', 'large scale precipitation scheme'), ],
        'constraints': [
            ('heading', 'value', 'Large scale preciptiation scheme name'),
            ('vocabulary', 'value', 'cmip6.atmos_large_scale_precipitation_scheme_name.%s' % version),
            ('???', 'from', 'cmip6.atmos_large_scale_precipitation_scheme_name', '1.1'), ]
    }


# Process Detail: Atmosphere: microphysics and precipitation: precipitating hydrometeors
def _atmos_large_scale_precipitation_hydrometeors():
    """Precipitating hydrometeors taken into account in the large scale precipitation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_large_scale_precipitation_hydrometeors', '1.N',
             'large scale precipitation hydrometeors'), ],
        'constraints': [
            ('heading', 'value', 'Large scale precipitation hydrometeor types'),
            ('vocabulary', 'value', 'cmip6.atmos_large_scale_precipitation_hydrometeor_types.%s' % version),
            ('selection', 'from', 'cmip6.atmos_large_scale_precipitation_hydrometeor_types', '1.N'), ]
    }


def _atmos_large_scale_precipitation_hydrometeor_types():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('liquid rain', None),
            ('snow', None),
            ('hail', None),
            ('graupel', None),
            ('other', None),
        ]
    }


# Process Detail: Atmosphere: microphysics and precipitation: microphysics scheme
def _atmos_cloud_microphysics_scheme():
    """Commonly used name of the microphysics parameterisation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('???', 'cmip6.atmos_cloud_microphysics_scheme', '1.1', 'cloud microphysics scheme'), ],
        'constraints': [
            ('heading', 'value', 'Cloud microphysics scheme name'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_microphysics_scheme_name.%s' % version),
            ('???', 'from', 'cmip6.atmos_cloud_microphysics_scheme_name', '1.1'), ]
    }


# Process Detail: Atmosphere: microphysics and precipitation: microphysics processes
def _atmos_cloud_microphysics_processes():
    """Precipitating hydrometeors taken into account in the large scale precipitation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_microphysics_processes', '1.N',
             'cloud microphysics processes'), ],
        'constraints': [
            ('heading', 'value', 'Cloud microphysics processes attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_microphysics_processes_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_cloud_microphysics_processes_attributes', '1.N'), ]
    }


def _atmos_cloud_microphysics_processes_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('mixed phase', None),
            ('cloud droplets', None),
            ('cloud ice', None),
            ('ice nucleation', None),
            ('water vapour deposition', None),
            ('effect of raindrops', None),
            ('effect of snow', None),
            ('effect of graupel', None),
            ('other', None),
        ]
    }


# Process: Atmosphere: cloud scheme
def _atmos_cloud_scheme():
    """ Characteristics of the cloud scheme"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Cloud Scheme'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution', ]),
            ('detailed_properties', 'include', [
                'cmip6.atmos_cloud_scheme_separate_treatment',
                'cmip6.atmos_cloud_scheme_cloud_overlap',
                'cmip6.atmos_cloud_scheme_processes', ]),
            ]
        }


# Process-detail: Atmosphere: cloud scheme: separate treatment
def _atmos_cloud_scheme_separate_treatment():
    """Different cloud schemes for the different types of clouds (convective, stratiform and boundary layer clouds)."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_scheme_separate_treatment', '1.1',
             'cloud scheme separated cloud treatment'), ],
        'constraints': [
            ('heading', 'value', 'Cloud scheme separate treatment attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_scheme_separate_treatment_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_cloud_scheme_separate_treatment_attributes', '1.1'), ]
    }


def _atmos_cloud_scheme_separate_treatment_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Yes', None),
            ('No', None),
        ]
    }


# Process-detail: Atmosphere: cloud scheme: cloud overlap
def _atmos_cloud_scheme_cloud_overlap():
    """Method for taking into account overlapping of cloud layers."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_scheme_cloud_overlap', '1.1', 'cloud scheme cloud overlap'), ],
        'constraints': [
            ('heading', 'value', 'Cloud scheme cloud overlap attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_scheme_cloud_overlap_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_cloud_scheme_cloud_overlap_attributes', '1.1'), ]
    }


def _atmos_cloud_scheme_cloud_overlap_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('random', None),
            ('none', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: cloud scheme: cloud processes
# Need to configure this to allow users to enter their own list of attributes
def _atmos_cloud_scheme_processes():
    """Cloud processes included in the cloud scheme (e.g. entrainment, detrainment, bulk cloud, etc.)."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('???', 'cmip6.atmos_cloud_scheme_processes', '1.1', 'cloud scheme processes'), ],
        'constraints': [
            ('heading', 'value', 'Cloud scheme cloud processes attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_scheme_processes_attributes.%s' % version),
            ('???', 'from', 'cmip6.atmos_cloud_scheme_processes_attributes', '1.N'), ]
    }


# Process-detail: Atmosphere: cloud scheme: sub-grid scale water distribution
# I don't know how to configure the class to expect the user to enter their own strings/integers/lists of strings
def _atmos_cloud_scheme_sub_grid_scale_water_distribution():
    """Sub-grid scale water distribution."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_type',
             '1.1', 'sub-grid scale water distribution type'),
            ('???', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_name',
             '1.1', 'sub-grid scale water distribution function name'),
            ('???', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_order',
             '1.1', 'sub-grid scale water distribution function type'),
            ('selection', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_convection',
             '1.N', 'sub-grid scale water distribution coupling with convection'), ],
        'constraints': [
            [('heading', 'value', 'sub-grid scale water distribution type attribute'),
                ('vocabulary', 'value',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_type_attribute.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_type_attribute',
                 '1.1'), ],
            [('heading', 'value', 'sub-grid scale water distribution function name'),
                ('vocabulary', 'value',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_name_attribute.%s' % version),
                ('???', 'value',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_name_attribute', '1.1'), ],
            [('heading', 'value', 'sub-grid scale water distribution function order'),
                ('vocabulary', 'value',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_order_attribute.%s' % version),
                ('???', 'from',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_order_attribute', '1.1'), ],
            [('heading', 'value', 'sub-grid scale water distribution coupling with convection attributes'),
                ('vocabulary', 'value',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_convection_attributes.%s' % version),
                ('???', 'from',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_convection_attributes', '1.N'), ], ]
    }


def _atmos_cloud_scheme_sub_grid_scale_water_distribution_type_attribute():
    """Approach used for cloud water content and fractional cloud cover"""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('prognostic', None),
            ('diagnostic', None),
        ]
    }


def _atmos_cloud_scheme_sub_grid_scale_water_distribution_function_name():
    """Commonly used name of the probability density function (PDF)
    representing the distribution of water vapour within a grid box."""
    return{
    }


def _atmos_cloud_scheme_sub_grid_scale_water_distribution_function_order():
    """Order of the function (PDF) used to represent subgrid scale water vapour distribution."""
    return{
    }


def _atmos_cloud_scheme_sub_grid_scale_water_distribution_convection_attributes():
    """Type(s) of convection that the formation of clouds is coupled with."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('coupled with deep', None),
            ('coupled with shallow', None),
            ('not coupled with convection', None),
        ]
    }


# Process: Atmosphere: cloud simulator
# How do I configure these classes to expect the user to enter their own strings/integers/lists of strings
def _atmos_cloud_simulator():
    """Characteristics of the cloud simulator"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Cloud Simulator'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_cloud_simulator_cosp_attributes',
                'cmip6.atmos_cloud_simulator_inputs_radar',
                'cmip6.atmos_cloud_simulator_inputs_lidar',
                'cmip6.atmos_cloud_simulator_isscp_attributes', ]),
            ]
        }


# Process-detail: Atmosphere: cloud simulator: COSP attributes
# I don't know how to configure the class to expect the user to enter their own strings/integers/lists of strings
def _atmos_cloud_simulator_cosp_attributes():
    """CFMIP Observational Simulator Package attributes"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_simulator_cosp_run_configuration', '1.1',
             'cloud simulator COSP run configuration'),
            ('???', 'cmip6.atmos_cloud_simulator_cosp_number_of_grid_points', '1.1',
             'cloud simulator COSP number of grid points'),
            ('???', 'cmip6.atmos_cloud_simulator_cosp_number_of_columns', '1.1',
             'cloud simulator COSP number of cloumns'),
            ('???', 'cmip6.atmos_cloud_simulator_cosp_number_of_levels', '1.1',
             'cloud simulator COSP number of levels'), ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator COSP run configuration'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_cosp_run_configuration.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_cosp_run_configuration', '1.1'), ],
            [('heading', 'value', 'Cloud simulator COSP number of grid points'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_cosp_number_of_grid_points.%s' % version),
                ('???', 'value', 'cmip6.atmos_cloud_simulator_cosp_number_of_grid_points', '1.1'), ],
            [('heading', 'value', 'Cloud simulator COSP number of columns'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_cosp_number_of_columns.%s' % version),
                ('???', 'from', 'cmip6.atmos_cloud_simulator_cosp_number_of_columns', '1.1'), ],
            [('heading', 'value', 'Cloud simulator COSP number of levels'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_cosp_number_of_levels.%s' % version),
                ('???', 'from', 'cmip6.atmos_cloud_simulator_cosp_number_of_levels', '1.1'), ], ]
    }


def _atmos_cloud_simulator_cosp_run_configuration():
    """Method used to run the CFMIP Observational Simulator Package"""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Inline', None),
            ('Offline', None),
            ('None', None),
        ]
    }


def _atmos_cloud_simulator_cosp_number_of_grid_points():
    """Number of grid points used by COSP."""
    return{
    }


def _atmos_cloud_simulator_cosp_number_of_columns():
    """Number of subcolumns used by COSP."""
    return{
    }


def _atmos_cloud_simulator_cosp_number_of_levels():
    """Number of model levels used by COSP."""
    return{
    }


# Process-detail: Atmosphere: cloud simulator: radar simulator
# Don't know how to add ability for users to enter a number for the radar frequency.
def _atmos_cloud_simulator_inputs_radar():
    """Characteristics of the cloud radar simulator"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('???', 'cmip6.atmos_cloud_simulator_inputs_radar_frequency', '1.1',
             'cloud simulator radar frequency'),
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_radar_type', '1.1',
             'cloud simulator radar type'),
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_radar_gas_absorption',
             '1.1', 'cloud simulator radar uses gas absorption'),
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_radar_effective_radius',
             '1.1', 'cloud simulator radar uses effective radius'), ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator radar frequency'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_radar_frequency.%s' % version),
                ('???', 'from', 'cmip6.atmos_cloud_simulator_inputs_radar_frequency', '1.1'), ],
            [('heading', 'value', 'Cloud simulator radar type'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_radar_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_radar_type', '1.1'), ],
            [('heading', 'value', 'Cloud simulator radar uses gas absorption'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_radar_gas_absorption.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_radar_gas_absorption', '1.1'), ],
            [('heading', 'value', 'Cloud simulator radar uses effective radius'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_radar_effective_radius.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_radar_effective_radius', '1.1'), ],
        ]
    }


def _atmos_cloud_simulator_inputs_radar_frequency():
    """CloudSat radar frequency."""
    return{
    }


def _atmos_cloud_simulator_inputs_radar_type():
    """Type of radar."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('surface', None),
            ('space bourne', None),
        ]
    }


def _atmos_cloud_simulator_inputs_radar_gas_absorption():
    """Does the radar simulator include gaseous absorption?"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('yes', None),
            ('no', None),
        ]
    }


def _atmos_cloud_simulator_inputs_radar_efective_radius():
    """Does the radar simulator use effective radius?"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('yes', None),
            ('no', None),
        ]
    }


# Process-detail: Atmosphere: cloud simulator: lidar simulator
def _atmos_cloud_simulator_inputs_lidar():
    """Characteristics of the cloud lidar simulator"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_lidar_ice_type', '1.1', 'cloud simulator lidar ice type'),
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_lidar_overlap', '1.N', 'cloud simulator lidar overlap'),
        ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator lidar ice type'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_lidar_ice_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_lidar_ice_type', '1.1'), ],
            [('heading', 'value', 'Cloud simulator lidar overlap'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_lidar_overlap.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_lidar_overlap', '1.N'), ], ]
    }


def _atmos_cloud_simulator_inputs_lidar_ice_type():
    """Ice particle shape in lidar calculations"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('ice spheres', None),
            ('ice non-spherical', None),
        ]
    }


def _atmos_cloud_simulator_inputs_lidar_overlap():
    """lidar overlap type"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('max', None),
            ('random', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: cloud simulator: ISSCP attributes        
def _atmos_cloud_simulator_isscp_attributes():
    """ISSCP Characteristics"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_simulator_isscp_top_height', '1.N',
             'cloud simulator ISSCP top height'),
            ('selection', 'cmip6.atmos_cloud_simulator_isscp_top_height_direction', '1.1',
             'cloud simulator ISSCP top height direction'), ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator ISSCP top height'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_isscp_top_height.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_isscp_top_height', '1.N'), ],
            [('heading', 'value', 'Cloud simulator ISSCP top height direction'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_isscp_top_height_direction.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_isscp_top_height_direction', '1.1'), ], ]
    }


def _atmos_cloud_simulator_isscp_top_height():
    """Cloud top height management"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('no adjustment', None),
            ('IR brightness', None),
            ('visible optical depth', None),
        ]
    }


def _atmos_cloud_simulator_isscp_top_height_direction():
    """Direction for finding the radiance determined cloud-top pressure. 
       Atmosphere pressure level with interpolated temperature equal to the radiance determined 
       cloud-top pressure."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('lowest altitude level', None),
            ('hightest altitute level', None),
        ]
    }


# Process: Atmosphere: gravity waves
def _atmos_gravity_waves():
    """ Characteristics of the parameterised gravity waves in the atmosphere, whether
     from orography or other sources."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Parameterised Gravity Waves'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_orographic_gravity_waves',
                'cmip6.atmos_non_orographic_gravity_waves', ]),
            ('detailed_properties', 'include', [
                'cmip6.atmos_gravity_waves_sponge_layer',
                'cmip6.atmos_gravity_waves_background',
                'cmip6.atmos_gravity_waves_sub_grid_scale_orography', ]), ]
        }


# Process-detail: Atmosphere: gravity waves: sponge layer
# May prove to be candidate for deprecation if top of atmos sponge layer turns out to
# already be covered by the atmos dynamical core process class
def _atmos_gravity_waves_sponge_layer():
    """Sponge layer in the upper levels in order to avoid gravity wave reflection at the top."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_gravity_waves_sponge_layer', '1.1', 'gravity waves sponge layer'), ],
        'constraints': [
            ('heading', 'value', 'Gravity wave sponge layer attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_gravity_waves_sponge_layer_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_gravity_waves_sponge_layer_attributes', '1.1'), ]
    }


def _atmos_gravity_waves_sponge_layer_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('None', None),
            ('Other', None),
        ]
    }


# Process-detail: Atmosphere: gravity waves: background distribution of waves
def _atmos_gravity_waves_background():
    """Background distribution of waves. """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_gravity_waves_background', '1.1', 'background wave distribution'), ],
        'constraints': [
            ('heading', 'value', 'Background wave distribution attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_gravity_waves_background_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_gravity_waves_background_attributes', '1.1'), ]
    }


def _atmos_gravity_waves_background_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('None', None),
            ('Other', None),
        ]
    }


# Process-detail: Atmosphere: gravity waves: subgrid-scale orography
def _atmos_gravity_waves_subgrid_scale_orography():
    """Subgrid scale orography effects taken into account."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'properties': [
            ('selection', 'cmip6.atmos_gravity_waves_subgrid_scale_orography', '1.N', 'subgrid-scale orography'), ],
        'constraints': [
            ('heading', 'value', 'sub-grid scale orography attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_gravity_waves_subgrid_scale_orography_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_gravity_waves_subgrid_scale_orography_attributes', '1.N'), ]
    }


def _atmos_gravity_waves_subgrid_scale_orography_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('effect on drag', None),
            ('effect on lifting', None),
            ('Other', None),
        ]
    }


# Algorithm: Atmosphere: gravity waves: orographic gravity waves
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: source mechanisms
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: calculation method
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: propagation scheme
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: dissipation scheme
def _atmos_orographic_gravity_waves():
    """Gravity waves generated due to the presence of orography."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.algorithm',
        'properties':
        [
            ('selection', 'cmip6.atmos_orographic_gravity_waves_source_mechanism',
                '1.N', 'Orographic gravity wave source mechanisms'),
            ('selection', 'cmip6.atmos_orographic_gravity_waves_calculation_method',
                '1.N', 'Orographic gravity wave calculation method'),
            ('selection', 'cmip6.atmos_orographic_gravity_waves_propagation_scheme',
                '1.1', 'Orographic gravity wave propogation scheme'),
            ('selection', 'cmip6.atmos_orographic_gravity_waves_dissipation_scheme',
                '1.1', 'Orographic gravity wave dissipation scheme'),
        ],
        'constraints': [
            [('heading', 'value', 'Orographic gravity wave source mechanisms'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_source_mechanisms.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_source_mechanisms', '1.N'), ],
            [('heading', 'value', 'Orographic gravity wave calculation method'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_calculation_method.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_calculation_mehtod', '1.N'), ],
            [('heading', 'value', 'Orographic gravity wave propogation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_propagation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_propagation_scheme', '1.1'), ],
            [('heading', 'value', 'Orographic gravity wave dissipation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_dissipation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_dissipation_scheme', '1.1'), ], ]
    }


def _atmos_orographic_gravity_wave_source_mechanisms():
    """Physical mechanisms generatic orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('linear mountain waves', None),
            ('hydraulic jump', None),
            ('envelope orography', None),
            ('statistical sub-grid scale variance', None),
            ('other', None),
        ]
    }


def _atmos_orographic_gravity_wave_calculation_method():
    """Calculation method for orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('non-linear calculation', None),
            ('more than two cardinal directions', None),
        ]
    }


def _atmos_orographic_gravity_wave_propagation_scheme():
    """Type of propagation scheme for orgraphic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('linear theory', None),
            ('non-linear theory', None),
            ('other', None),
        ]
    }       


def _atmos_orographic_gravity_wave_dissipation_scheme():
    """Type of dissipation scheme for orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('total wave', None),
            ('single wave', None),
            ('spectral', None),
            ('linear', None), 
            ('other', None),
        ]
    }

                
# Algorithm: Atmosphere: gravity waves: non-orographic gravity waves
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: source mechanisms
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: calculation method
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: propagation scheme
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: dissipation scheme
def _atmos_non_orographic_gravity_waves():
    """Gravity waves generated by non-orographic processes."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.algorithm',
        'properties': [
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_source_mechanisms',
                '1.N', 'Non-orographic gravity wave source mechanisms'),
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_calculation_method',
                '1.N', 'Non-orographic gravity wave calculation method'),
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_propagation_scheme',
                '1.1', 'Non-orographic gravity wave propogation scheme'),
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_dissipation_scheme',
                '1.1', 'Non-orographic gravity wave dissipation scheme'), ],
        'constraints': [
            [('heading', 'value', 'Non-orographic gravity wave source mechanisms'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_source_mechanisms.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_source_mechanisms', '1.N'), ],
            [('heading', 'value', 'Non-orographic gravity wave calculation method'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_calculation_method.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_calculation_mehtod', '1.N'), ],
            [('heading', 'value', 'Non-orographic gravity wave propagation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_propogation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_propogation_scheme', '1.1'), ],
            [('heading', 'value', 'Non-orographic gravity wave dissipation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_dissipation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_dissipation_scheme', '1.1'), ], ]
    }


def _atmos_non_orographic_gravity_wave_source_mechanisms():
    """Physical mechanisms generating non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('convection', None),
            ('precipitation', None),
            ('background spectrum', None),
            ('other', None),
        ]
    }


def _atmos_non_orographic_gravity_wave_calculation_method():
    """Calculation method for non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('spatially dependent', None),
            ('temporally dependent', None),
            ]
    }


def _atmos_non_orographic_gravity_wave_propagation_scheme():
    """Type of propagation scheme for non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('linear theory', None),
            ('non-linear theory', None),
            ('other', None),
        ]
    }       


def _atmos_non_orographic_gravity_wave_dissipation_scheme():
    """Type of dissipation scheme for non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('total wave', None),
            ('single wave', None),
            ('spectral', None),
            ('linear', None), 
            ('other', None),
        ]
    }
