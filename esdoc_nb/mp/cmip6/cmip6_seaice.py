__author__ = 'BNL28'
version = '0.0.1'

#
# This is the candidate standard vocabulary for CMIP6 sea ice.

# Note that the classes and enums need to be mixed to make it possible to easily develop these
# ab initio in python. If we develop a different toolchain, we can split them.

# Note that vocab_status attribute is used for two reasons: the primary reason is to
# help the test system known what needs to be done in vocabulary development. Classes
# in the CMIP6 package without the attribute, or at the wrong level, are deemed to be
# incomplete. It is also used in the notebook meta system to indicate that classes
# carrying this attribute will not be abstract, and will not carry properties.

#
# Domain definition
#

def _sea_ice_domain():
    """ Characteristics of the sea ice simulation"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.scientific_domain',
        'constraints':[
            ('realm','value', 'Sea Ice'),
            ('simulates', 'include', ['cmip6.sea_ice_properties', 'cmip6.sea_ice_thermodynamics', 'cmip6.sea_ice_dynamics']),
        ]
    }

#
# Processes follow
#

def _sea_ice_properties():
    """ This is the catch all generic sea ice description """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints':[
            ('name', 'value', 'Sea Ice'),
            ('detailed_properties','include', ['cmip6.sea_ice_layering','cmip6.sea_ice_grid']),
    ]
    }

def _sea_ice_thermodynamics():
    """ Characteristics of sea ice thermodynamics processes"""
    return {
        'type': 'class',
        'vocab_status':'initial',
        'base': 'science.process',
        'constraints': [
            ('name','value','Sea Ice Thermodynamics'),
            ('algorithm_properties','include',[
                    'cmip6.sea_ice_thermodynamics_budget',
                    'cmip6.sea_ice_ocean_to_ice_basal_heat_flux']),
            ('detailed_properties','include',[
                    'cmip6.sea_ice_snow_processes',
                    'cmip6.sea_ice_radiative_processes',
                    'cmip6.sea_ice_vertical_heat_diffusion',
                    'cmip6.sea_ice_brine_inclusions',
                    'cmip6.sea_ice_thermodynamics_additional_processes',]),
            ]
     }

def _sea_ice_dynamics():
    """ Characteristics of the sea ice dynamics process"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name','value','Sea Ice Dynamics'),
            ('detailed_properties','include',['cmip6.sea_ice_horizontal_advection',
                                     'cmip6.sea_ice_transport_in_thickness_space',
                                     'cmip6.sea_ice_redistribution',
                                     'cmip6.sea_ice_rheology',]),
        ]
    }

#
# Sub process collection detail follows.
#
# We would not expect the comparator to go below comparisons at the sub-process
# level.
#
# Enums follow processes which use them. Not all process use them.
#
def _sea_ice_snow_processes():
    """ """
    return {
        'type': 'class',
        'vocab_status':'initial',
        'base': 'science.process_detail',
        'properties':[
            ('selection','cmip6.sea_ice_possible_snow_processes','1.N','Possible snow processes'),
        ],
        'constraints':[
            ('heading', 'value', 'Snow processes in sea ice thermodynamics'),
            ('vocabulary', 'value', 'cmip6.sea_ice_possible_snow_processes.%s' % version),
            ('selection','from','cmip6.sea_ice_possible_snow_processes','1.N'),
        ]
    }

def _sea_ice_possible_snow_processes():
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members':[
            ('single-layered heat diffusion', None),
            ('multi-layered heat diffusion', None),
            ('snow aging scheme', None),
            ('snow ice scheme', None),
        ]
    }

def _sea_ice_thermodynamics_budget():
    """ """
    return {
        'type': 'class',
        'vocab_status':'initial',
        'base': 'science.algorithm',
        'constraints':[
            ('heading', 'value',
                'List of variables associated with closing the sea ice thermodynamic budget')
            ],
    }

def _sea_ice_radiative_processes():
    """ """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'constraints':[
            ('heading', 'value', 'Radiative processes in sea ice thermodynamics'),
            ('vocabulary', 'value', 'cmip6.sea_ice_possible_radiative_processes.%s' % version),
            ('selection','from','cmip6.sea_ice_possible_radiative_processes','1.N'),
        ],
    }

def _sea_ice_possible_radiative_processes():
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members':[
            ('solar radiation','Solar radiation propagates through ice'),
            ('surface albedo','Method used to handle surface albedo '),
        ]
    }


def _sea_ice_vertical_heat_diffusion():
    """ """
    return {
        'type': 'class',
        'base': 'science.process_detail',
        'vocab_status':'initial',
        'constraints':[
            ('heading', 'value', 'Method used for vertical heat diffusion. Layering? '),
            ('vocabulary', 'value', 'cmip6.sea_ice_vertical_heat_diffusion_types.%s' % version),
            ('selection','from', 'cmip6.sea_ice_vertical_heat_diffusion_types','1.1'),
        ],
    }

def _sea_ice_vertical_heat_diffusion_types():
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('one-layer', None),
            ('multi-layer irregular grid', None),
            ("two-layer", 'Semtner 76'),
            ('multi-layer regular grid', 'with more than two layers'),
        ]
    }

def _sea_ice_ocean_to_ice_basal_heat_flux():
    """ """
    return {
        'type': 'class',
        'base': 'science.algorithm',
        'vocab_status': 'initial',
        'constraints': [
            ('heading', 'value',
                'Implementation detail for the ocean to ice basal heat flux.'),
            ('implementation_overview','value','(Replace: whether prescribed or calculated, and how)'),
            ('prognostic_variables', 'hidden'),
            ('diagnostic_variables', 'hidden'),
        ]
    }


def _sea_ice_brine_inclusions():
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'constraints':[
            ('heading', 'value', 'Method by which brine inclusions are handled'),
            ('vocabulary', 'value','cmip6.sea_ice_brine_inclusions_thermal_properties.%s' % version),
            ('selection','from','cmip6.sea_ice_brine_inclusions_thermal_properties','1.1'),
            ],
    }

def _sea_ice_brine_inclusions_thermal_properties():
    """ Method of handling the thermal properties of brine inclusions in sea ice """
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'is_open': False,
        'base': None,
        'members': [
            ('None','No brine inclusions included in sea ice properties'),
            ('Heat Reservoir','Brine inclusions treated as a heat reservoir'),
            ('Thermal Fixed Salinity','Thermal properties depend on S-T (with fixed salinity)'),
            ('Thermal Varying Salinity','Thermal properties depend on S-T (with varying salinity'),
        ]
    }

def _sea_ice_horizontal_advection():
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'constraints':[
            ('heading','value','Method of horizontal advection? '),
            ('vocabulary','value','cmip6.sea_ice_horizontal_advection_types.%s' % version),
            ('selection','from','cmip6.sea_ice_horizontal_advection_types','1.1'),
        ],
    }

def _sea_ice_horizontal_advection_types():
     return {
        'type': 'enum',
        'vocab_status': 'initial',
        'is_open': False,
        'base':None,
        'members': [
            ('Incremental Re-mapping','including Semi-Lagrangian'),
            ('Prather', None),
            ('Other', None)
        ]
     }

def _sea_ice_transport_in_thickness_space():
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'constraints':[
            ('heading','value','Method of ice migration'),
            ('vocabulary','value','cmip6.sea_ice_thickness_transport_types.%s' % version),
            ('selection','from','cmip6.sea_ice_thickness_transport_types','1.1'),
            ],
    }

def _sea_ice_thickness_transport_types():
     return {
        'type': 'enum',
        'vocab_status': 'initial',
        'is_open': False,
        'base':None,
        'members': [
            ('Incremental Re-mapping', None),
            ('Eulerian', None),
            ('Other', None)
        ]
     }


def _sea_ice_redistribution():
       return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process_detail',
        'constraints':[
            ('heading', 'value', 'Information about how sea ice is redistributed beyond advection'),
            ('vocabulary', 'value', 'cmip6.sea_ice_redistribution_types.%s' % version),
            ('selection','from','cmip6.sea_ice_redistribution_types','1.N'),
            ],
    }

def _sea_ice_redistribution_types():
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'is_open': False,
        'base':None,
        'members': [
            ('Rafting', None),
            ('Ridging', None),
            ('Ice Strength Formulation', 'Details of formulation and usage'),
            ('Other', None),
        ]
    }

def _sea_ice_rheology():
    return {
        'type': 'class',
        'base': 'science.process_detail',
        'vocab_status': 'initial',
        'constraints':[
            ('heading','value','#FIXME'),
            ('vocabulary','value','cmip6.sea_ice_rheology_types.%s' % version),
            ('selection','from','cmip6.sea_ice_rheology_types','1.1'),
            ],
    }

def _sea_ice_rheology_types():
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'is_open': False,
        'base': None,
        'members': [
            ('free-drift', None),
            ('Mohr-Coloumb', None),
            ('visco-plastic', None),
            ('elastic-visco-plastic','EVP'),
            ('granular', None),
            ('other', None)
        ]
    }

def _sea_ice_thermodynamics_additional_processes():
    return {
        'type': 'class',
        'base': 'science.process_detail',
        'vocab_status': 'initial',
        'constraints':[
            ('heading','value','A set of additional important sea ice processes'),
            ('vocabulary','value','cmip6.sea_ice_thermodynamics_additional_process_types.%s' % version),
            ('selection','from','cmip6.sea_ice_thermodynamics_additional_process_types','1.N'),
            ],
    }


def _sea_ice_thermodynamics_additional_process_types():
    """ A set of sub-processes which might be included in sea ice thermodynamics"""
    return {
        'type': 'enum',
        'is_open': False,
        'vocab_status': 'initial',
        'base': None,
        'members': [
            ('New Ice Formation','Description of method'),
            ('Ice Lateral Melting','Sea ice lateral melting: Method?'),
            ('Ice Surface Sublimation','Surface sublimation of water: Method?'),
            ('Ice Radiation Transmission','Solar radiation transmission through ice: Method?'),
            ('Water ponds','Water ponds supported: How?'),
        ]
    }

def _sea_ice_layering():
     return {
        'type': 'class',
        'base': 'science.process_detail',
        'vocab_status': 'initial',
        'constraints': [
            ('heading','value','Method used to represent sea ice layering'),
            ('vocabulary','value','cmip6.sea_ice_layering_types.%s' % version),
            ('selection','from','cmip6.sea_ice_layering_types', '1.1'),
            ],
    }


def _sea_ice_layering_types():
    """Types of methods of layering in sea ice models"""
    return {
        'type': 'enum',
        'is_open': False,
        'base':None,
        'members': [
            ('2-levels', 'Simulation uses two layers.'),
            ('Multi-level','Simulation uses more than two layers (please enter number of layers using key,value).'),
            ('Ice-Types','Simulation does not use layers, but has multiple ice types per grid cell'),
        ]
    }

def _sea_ice_grid():
    """ How the sea ice is horizontally discretized."""
    return {
        'type': 'class',
        'base': 'science.process_detail',
        'vocab_status': 'initial',
        'constraints': [
            ('heading','value','How the sea ice is horizontally discretized'),
            ('vocabulary','value','cmip6.sea_ice_grid_types.%s' % version),
            ('selection','from','cmip6.sea_ice_grid_types','1.1'),
            ],
    }


def _sea_ice_grid_types():
    """Types of possible horizontal discretization for sea ice"""
    return {
        'type': 'enum',
        'is_open': False,
        'base': None,
        'members': [
            ('Ocean Grid',"Sea Ice discretized on ocean grid"),
            ('Atmosphere Grid',"Sea Ice discretized on atmosphere grid"),
            ('Own Grid',"Sea Ice discretized on own grid"),
        ]
    }