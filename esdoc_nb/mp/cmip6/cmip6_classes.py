__author__ = 'BNL28'

# This is the set of classes for which CMIP6 specialisations exist.
#
# When these classes are used, the CMIP6 scientific vocabularies will automatically be
# imported.
#
# Data creation tools will need to make choosing these classes instead of vanilla classes
# possible/mandatory via their interface ...
#

template_classes = {'cmip6': ['cmip6.sea_ice_domain',]}

def _cmip6_model():
    """ CMIP6 models are constrained to have only scientific descriptions"""
    return {
        'type': 'class',
        'base': 'software.model',
        'is_abstract': False,
        'properties': [
            ('sea_ice_domain_description', 'linked_to(cmip6.sea_ice_domain)', '1.1',
                'Sea Ice domain description'),
        ],
        'vocab_status': 'initial',
        'constraints': [
            ('scientific_domain','hidden'),
            ('coupled_software_components', 'hidden'),
            ('internal_software_components', 'hidden'),
            ('coupler', 'hidden'),
        ]
    }


def _cmip6_ensemble():
    """ Ensemble Definition Class, provides r/i/p definitions """
    return {
        'type': 'class',
        'base': 'activity.ensemble',
        'is_abstract': False,
        'vocab_status': 'initial',
        'properties': [
            ('r_defined_by', 'activity.realisationAxis', '1.1',
                'Description of "r" ensemble axis members'),
            ('i_defined_by', 'activity.initialisationAxis', '1.1',
                'Description of "i" ensemble axis members'),
            ('p_defined_by', 'activity.perturbedPhysicsAxis', '1.1',
                'Description of "p" ensemble axis members'),
            ('s_defined_by', 'activity.memberDescription', '0.1',
                'Description of start date ensemble axis (if appropriate)'),
            ],
        'constraints': [
            ('has_ensemble_axes', 'hidden')
        ]
    }

