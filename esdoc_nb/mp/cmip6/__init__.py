__author__ = 'BNL28'

from inspect import getmembers, isfunction

import cmip6_classes as classes6

import cmip6_enums as enums6

import cmip6_seaice as seaice

__all__ = ['get_constructors']

def get_constructors():
    """ Returns the complete set of cim class and enum constructor functions necessary for the CMIP6
    vocabulary (note this does not return the dictionaries, but the functions used to create them).
    return: dictionary of functions.
    """
    pdict = {'classes6': classes6, 'enums6': enums6, 'seaice': seaice}
    all_functions = []
    for p in pdict:
        module = pdict[p]
        function_list = [o for o in getmembers(module) if isfunction(o[1])]
        for f in function_list:
            all_functions.append(f)
    return all_functions


