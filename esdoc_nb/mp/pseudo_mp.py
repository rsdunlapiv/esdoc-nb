import unittest
import uuid
import collections
import copy
from inspect import getmembers, isfunction
from datetime import datetime
import pyesdoc.ontologies.cim.v2 as esdoc

# CIM imports follow

import cim2.shared_classes_time as shared_time_c
import cim2.shared_classes as shared_c
import cim2.data_classes as data_c
import cim2.software_classes as software_c
import cim2.software_enums as software_e
import cim2.platform_classes as platform_c
import cim2.activity_classes as activity_c
import cim2.designing_classes as designing_c
import cim2.science_classes as science_c
import cim2.science_enums as science_e
import cim2.drs_entities as drs_c

# vocab imports follow
import cmip6

__author__ = 'BNL28'

# known CIM classes:

class_packages = {'activity': activity_c,
            'shared': shared_c,
            'shared_time': shared_time_c,
            'software': software_c,
            'platform': platform_c,
            'science': science_c,
            'data': data_c,
            'drs': drs_c,
            'designing': designing_c,
            }

enum_packages = {'software': software_e,
                 'science': science_e,
                 }

classmap = {'int': [int, ],
            'str': [str, unicode],
            'datetime': [str, ],
            'float': [float, ],
            'bool': [bool, ],
            'text': [str, unicode]}

# These are the classes where vocabulary extensions *can* takeover ... if implemented

extension_points = {'cmip6':
                        {'Model': 'Cmip6Model',
                         'ensemble': 'Cmip6Ensemble'}
                    }

def make_class_name(dname):
    """ Makes a class name out of a function name """
    # Used to go from the esdoc-mp world direct to the notebook meta world.
    # Everything is abc_def in esdoc_mp, but here it's AbcDef.
    if dname in classmap: return dname
    x = dname.split('_')
    y = [s.capitalize() for s in x]
    return ''.join(y)

def fix_fully_qualified_name(dname):
    """ Fix the class name within a package.name string """
    # Convert package.abc_def to package.AbcDef
    s=dname.split('.')
    if len(s) == 1:
        return make_class_name(dname)
    elif len(s) == 2:
        return '.'.join([s[0], make_class_name(s[1])])

def fix_all_names(dname):
    """ Fix any name, with or without linked_to """
    if dname.startswith('linked_to'):
        return 'linked_to(%s)' % fix_fully_qualified_name(dname[10:-1])
    else:
        return fix_fully_qualified_name(dname)

def fix_property_names(p):
    """ Given a UML property definition string, fix the property definition """
    m = list(p)
    m[1] = fix_all_names(m[1])
    return tuple(m)

def package_split(cimtype):
    """ Split a cimtype into a package part and a native class part """
    if cimtype.startswith('linked_to'):
        c = cimtype[10:-1].split('.')
    else:
        c = cimtype.split('.')
    cimtype = c[-1]
    if len(c)==2:
        return c[0],cimtype
    else: return None,cimtype

def get_from_modules(pdict):
    """ Load constructors from a dictionary of modules """
    c = {}
    pkg = {}
    for p in pdict:
        module = pdict[p]
        function_list = [o for o in getmembers(module) if isfunction(o[1])]
        pkg[p] = []
        for f in function_list:
            key, constructor = function2dict(f)
            c[key] = constructor
            pkg[p].append(key)
    return c, pkg

def function2dict(f):
    """ Used to go from the cim defined as functions to the cim defined as a dictionary
    :param f: a constructor function
    :return: (key,c) a cimtype and it's constructor dictionary
    """
    key = make_class_name(f[0])
    try:
        c = f[1]()
        if f[1].__doc__ != '':
            c['doc'] = f[1].__doc__
        else:
            # If no real doc string,
            # assume the class name is useful in it's own right
            c['doc'] = key.replace('_',' ')
        c['cimType'] = key
        # The metamodel expects these, but they get in the way of
        # vocabulary construction (since they have no value in
        # normal vocabularies.
        if 'vocab_status' in c:
            if 'properties' not in c:
                c['properties'] = []
            if 'is_abstract' not in c:
                c['is_abstract'] = False
    except Exception, e:
        raise Exception, str(e)+'( for %s)' % key
    return key, c


class cimFactory(object):

    """ This class defines the CIM eco system and the internal classes, and
    allows one to build class instances."""

    # Try and insulate the notebook world from the esdoc-mp world by ensuring
    # that in esdoc land, everything that was abc_def is now AbcDef.

    def __init__(self, default=True):

        """ CIM eco system constructor. Default is to construct the
        basic CIM. If not default, construct as an empty factory."""

        self.classes = {}
        self.enums = {}
        self.packages = {}

        if default:
            self.extend(class_packages)
            self.extend(enum_packages)

    def build(self, klass):
        """ Makes and returns an instance of klass """
        if klass in self.classes:
            return umlClass(self.classes[klass], self.fullset)
        elif klass in self.enums:
            return umlEnum(self.enums[klass])
        else:
            raise ValueError('Cannot find constructor for %s in factory' % klass)

    def __contains__(self, klass):
        """ Allows one to determine if klass is in the CIM system """
        if klass not in self.classes and klass not in self.enums:
            return False
        return True

    def base(self, klass, follow=True):
        """For a given cim type, find base classes"""
        r = []
        if klass in self.classes:
            d = self.classes[klass]
        elif klass in self.enums:
            d = self.enums[klass]
        else:
            raise ValueError('Unexpected type for base: %s' % klass)
        if d['base']:
            b = package_split(d['base'])[1]
            r.append(b)
            if follow:
                r += self.base(b)
        return r

    def find_associates(self, klass, get_base=True, get_properties=True):
        """ For a given CIM class, klass, find all other classes which
        are associated *from* that class.
        If get_base, then include all base classes,
        If get_properties, then include all property classes.
        """
        extras = []
        if klass in self.enums: return extras
        assert klass in self.classes,'What to do with %s' % klass
        model = self.classes[klass]
        if get_base and model['base']:
            extras += self.base(model['cimType'])
        if get_properties:
            for p in model['properties']:
                if p[1] in self and p[1] not in extras:
                    extras.append(p[1])
        return extras

    def isdoc(self, klass):
        """ Convenience method to return whether or not a klass has metadata, ie
        conforms to the CIM2 document stereotype, without needing to build the
        class first."""
        if klass not in self: return False
        if klass in self.enums: return False
        if self.classes[klass]['is_abstract']: return False
        if 'meta' in [m[0] for m in self.classes[klass]['properties']]: return True
        for k in self.base(klass):
            if 'meta' in [m[0] for m in self.classes[k]['properties']]: return True
        return False

    def extend(self, package_list, api_fix=True):
        """  Extend to include building domain specific classes, e.g. cmip6
        :param package_list: A list of domain specific class constructors.
        """
        constructors, packages = get_from_modules(package_list)

        self._load_constructors(constructors, api_fix)

        for p in packages:
            if p in self.packages:
                self.packages[p] += packages[p]
            else:
                self.packages[p] = packages[p]

        self.fullset = self.classes.copy()
        self.fullset.update(self.enums)

    def _load_constructors(self, constructors, api_fix):
        """ Load constructors into internal class and enum dictionaries """
        # At the moment classes and enums are mixed
        for k in constructors:
            if constructors[k]['type'] == 'class':
                if api_fix:
                    self.classes[k] = self._apifix(constructors[k])
                else:
                    self.classes[k] = constructors[k]
            elif constructors[k]['type'] == 'enum':
                self.enums[k] = constructors[k]

    def add_package_by_functions(self, package_name, package_functions, api_fix=True):
        """ Add one package and its constructors from the basic function definitions.
        Used for cmip6 type package extensions.
        :param package_name: A new package
        :param package_constructors: It's corresponding constructors
        :return: None
        """
        constructors = {}
        self.packages[package_name] = []
        for f in package_functions:
            key, c = function2dict(f)
            constructors[key] = c
            self.packages[package_name].append(key)
        self._load_constructors(constructors, api_fix)

        self.fullset = self.classes.copy()
        self.fullset.update(self.enums)

    def _apifix(self, constructor):
        """ Fix all the properties in constructor to use my notebook style names """

        def pfix(prop):
            new = []
            for p in prop:
                new.append(fix_property_names(p))
            return new

        constructor['properties'] = pfix(constructor['properties'])
        if 'alternatives' in constructor:
            constructor['alternatives'] = [
                make_class_name(n) for n in constructor['alternatives']]
        if 'base' in constructor:
            if constructor['base'] is not None:
                constructor['base'] = fix_fully_qualified_name(constructor['base'])
        return constructor





class umlProperty(object):
    """ Implements a property which follows our pythonic uml property assertions.
    Property can be constrained to an unchangeable value by construction"""
    def __init__(self, attribute, value=None):
        """
        :param attribute: a metamodel property statement (e.g. ('name','type','1.1','docs')
        :param value: an initial value of the attribute (making this property fixed).
        :return: umlProperty
        """
        self.uml_attribute = attribute
        # make sure it's a valid property these assertions define our property metamodel
        try:
            assert isinstance(self.name, str) or isinstance(self.name, unicode),'Name Failure'
            assert self.cardinality in ['0.1', '1.1', '1.N', '0.N'],"Cardinality Failure"
        except AssertionError, e:
            raise ValueError('%s is not a valid property definition (%s)' %
                                (attribute,  str(e)))
        if self.single_valued:
            self.__value = None
        else:
            self.__value = []

        if value:
            self.__value = value
            self.constrained = True
        else:
            self.constrained = False

    @property
    def isFixed(self):
        """Constraint property may make this uml property unchangeable """
        if self.constrained: return 1
        return 0

    @property
    def value(self):
        return self.__value

    @property
    def name(self):
        return self.uml_attribute[0]

    @property
    def target(self):
        t = self.uml_attribute[1]
        if t.startswith('linked_to'):
            t = t[10:-1]
        tt=t.split('.')
        if len(tt) == 2:
            return tt[1]
        else: return t

    @property
    def fulltarget(self):
        """ The target including package name"""
        t = self.uml_attribute[1]
        if t.startswith('linked_to'):
            t = t[10:-1]
        return t

    @property
    def cardinality(self):
        return self.uml_attribute[2]

    @property
    def doc(self):
        return self.uml_attribute[3]

    @property
    def single_valued(self):
        return self.cardinality in ['0.1', '1.1']

    @property
    def valid(self):
        if self.cardinality.startswith('1'):
            if self.value in [None, []]:
                return 0
        return 1

    @property
    def islink(self):
        if self.uml_attribute[1].startswith('linked_to'):
            return 1
        return 0

    def __myType(self, other):
        """Check other object for consistency with me """
        if self.target in classmap:
            if other.__class__ not in classmap[self.target]:
                if self.target == 'int' and other.__class__ == float:
                    if float(int(other)) == other: return 1
                return 0
            return 1
        if not (isinstance(other, umlClass) or isinstance(other, umlEnum)): return 0
        if self.islink and isCIMinstance(other, 'OnlineResource'): return 1
        if isCIMinstance(other, self.target): return 1
        return 0

    def set(self,v):
        """ Set value """

        def wrongtype(v):
            """ Convenience method for error message"""
            if isinstance(v, umlClass) or isinstance(v, umlEnum):
                msgname = v.cimType
            else:
                msgname = v
            raise ValueError(
                    'Attempt to set <%s> with wrong type <%s>' % (self.name, msgname))

        if self.isFixed:
            return AttributeError("Attempt to change constrained fixed value %s"%self)

        if self.single_valued:
            if v is None:
                self.__value = None
                return
            if self.__myType(v):
                self.__value = v
            else:
                wrongtype(v)
        else:
            if isinstance(v,list):
                if v == []:
                    self.__value = v
                else:
                    for vv in v:
                        if not self.__myType(vv):
                            raise ValueError('Invalid data %s(%s) for %s'%(vv,v,self.target))
                    self.__value = v
            else:
                raise ValueError('Attempt to set a value to a list property')

    def append(self,v):
        """ Append value if appropriate """
        if self.single_valued:
            raise ValueError('Cannot append to %s'%self.name)
        self.__value.append(v)

    def reset_list(self,v):
        if self.single_valued:
            raise ValueError('Cannot reset list for %s'% self.name)
        self.__value = v

    def __eq__(self, other):
        if self.uml_attribute != other.uml_attribute: return 0
        if self.value != other.value: return 0
        return 1

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return str(self.uml_attribute)

# FIXME: Need to introduce support for a constraint language
# Currently want support for
# cardinality and value constraints.

class umlClass(object):
    """ Implements a class which follows our pythonic uml class assertions"""

    def __init__(self, constructor, cim_constructor_set):
        """
        :param constructor: A dictionary of class characteristics from the known classes in
        :param the cim_constructor_set
        """
        # uml attributes
        super(umlClass, self).__setattr__('attributes', collections.OrderedDict())

        # set of all known classes
        self.known = cim_constructor_set

        # class hierarchy:
        self.base = []
        # alternative classes (including sub-classes):
        self.alternatives = []
        # documentation for this class:
        self.doc = ''
        # serialisations, including print methods
        self.serialisations = []

        self.cimType = constructor['cimType']

        self._parseConstructor(constructor)

        if self.cimType in self.alternatives:
            index = self.alternatives.index(self.cimType)
            self.alternatives[index] = constructor['base']

        if 'uid' in self.attributes:
            # uniqueness better than randomness
            self.uid=str(uuid.uuid1())

        # other known attributes which we want to keep
        for a in ['pstr','version']:
            if a in constructor:
                setattr(self,a,constructor[a])
                
        # assert metamodel
        checkmetamodel(constructor)
        
    @property
    def isDocument(self):
        """Implementing the document stereotype"""
        if 'meta' in self.attributes:
            return 1
        else:
            return 0

    def __setattr__(self, k, v):
        """Override setattribute to ensure we have valid attributes
        where an attribute is a cim attribute"""
        if k in self.attributes:
            self.attributes[k].set(v)
        else:
            super(umlClass, self).__setattr__(k, v)

    def __getattribute__(self, item):
        """ Override getattribute to get items from the attribute
        store if appropriate """
        try:
            attributes = super(umlClass, self).__getattribute__('attributes')
        except AttributeError:
            # This shouldn't arise in normal execution, but when an instance is copied
            # using deepcopy, it somehow can ... although I don't know how the
            # instance can be constructed without using the constructor.
            attributes = []
        if item in attributes:
            return attributes[item].value
        else:
            return super(umlClass, self).__getattribute__(item)

    def _parseConstructor(self,constructor):
        """Parse constructor and load attributes"""

        # start with class hierarchy
        baseClass = constructor['base']
        if baseClass is not None:
            # Mark defines base classes with package included,
            # I don't ... so handle both for now.
            bc = baseClass.split('.')
            if len(bc) == 1:
                bc = bc[0]
            else:
                bc = bc[1]
            self._parseConstructor(self.known[bc])
            self.base.append(bc)

        # collect constraints
        constraintSet = {}
        if 'constraints' in constructor:
            for constraint in constructor['constraints']:
                constraintSet[constraint[0]] = constraint[1:]

        # now deal with the uml attributes
        for a in constructor['properties']:
            p = umlProperty(a)
            self.attributes[a[0]] = p

        # now go back and deal with any constraints:
        for c in constraintSet:
            o = self.attributes[c].uml_attribute
            p = self._constrained(o, constraintSet[c])
            if p is None:
                del self.attributes[c]
            else:
                self.attributes[c] = p

        self.constraints = constraintSet

        # doc string is a special case:
        assert 'doc' in constructor
        self.doc += ':%s' % constructor['doc']

        # aggregate alternatives
        if 'alternatives' in constructor:
            self.alternatives += constructor['alternatives']

    def _constrained(self, a, constraint):
        """ Provides class support for constraints.
        Currently understands constraints of the form
            ('description','cardinality','1.1')
            ('description','value','Unchangeable string')
            ('description','hidden')
            ('property','includes',[list of classes])
        """
        ac = list(a)
        assert constraint[0] in ['cardinality','value','hidden','include','from']
        if constraint[0] == 'cardinality':
            ac[2] = constraint[1]
            return umlProperty(ac)
        elif constraint[0] == 'value':
            return umlProperty(ac, value=constraint[1])
        elif constraint[0] == 'hidden':
            return None
        elif constraint[0] == 'include':
            prop = umlProperty(ac)
            assert not prop.single_valued
            assert isinstance(constraint[1],list)
            for x in constraint[1]:
                prop.append(makeCimInstanceFromSet(fix_fully_qualified_name(x), self.known))
            return prop
        elif constraint[0] == 'from':
            ac[1] = fix_fully_qualified_name(constraint[1])
            if len(constraint) == 3:
                ac[2] = constraint[2]
            return umlProperty(ac)

    def validate(self, quiet=False):
        """ Are all the required CIM attributes present?"""
        missing=[]
        for p in self.attributes:
            if not self.attributes[p].valid: missing.append(p)
        if missing == []:
            return 1
        else:
            if quiet:
                return 0
            else:
                raise ValueError('%s missing attributes %s' % (self.cimType, missing))


    @property
    def links(self):
        """Method to return all the outbound cim links for this instance as triples"""
        links = []
        for p in self.attributes:
            umlp = self.attributes[p]
            if umlp.islink and umlp.value:
                if umlp.single_valued:
                    value = (self, umlp.name, umlp.value)
                    links.append(value)
                else:
                    for v in umlp.value:
                        value = (self, umlp.name, v)
                        links.append(value)
        return links

    @property
    def pyesdoc(self):
        """ Method to return a pyesdoc version for printing, storing etc """
        # Start by instantiating a pyesdoc class and returning that, this is a work in progress
        try:
            klass = getattr(esdoc, self.cimType)
        except:
            raise ValueError('Cannot instantiate esdoc class %s ' % self.cimType)
        esdoc_class = klass()
        for a in self.attributes:
            umlp = self.attributes[a]
            value = umlp.value
            if value is not None:
                if umlp.target not in classmap:
                    if umlp.single_valued:
                        value = value.pyesdoc
                    else:
                        value = [v.pyesdoc for v in value]
            if 0: print 'Setting %s with %s' % (umlp.target, value)
            setattr(esdoc_class, umlp.name, value)
        return esdoc_class


    def __str__(self):
        """ String version """
        return make_string(self)

    def __eq__(self,other):
        """ Class and member equality """
        if not isinstance(other, self.__class__): return 0
        if self.cimType != other.cimType: return 0
        if len(self.__dict__) != len(other.__dict__): return 0
        for k in self.__dict__:
            if getattr(self, k) != getattr(other, k): return 0
        return 1

    def __ne__(self,other):
        """ Class and member inequality """
        return not self == other


class umlEnum(object):
    """ A CIM vocabulary """

    def __init__(self, constructor):
        """ Construct and return a vocabulary ENUM from an mp entity description"""
        self.members = {}
        for m in constructor['members']:
            self.members[m[0]] = m[1]
        if constructor['base'] is not None:
            raise ValueError('Code for subclassing enums not yet available')
        for a in constructor:
            if a not in ['base', 'members']:
                setattr(self, a, constructor[a])
        self.value = self.members.keys()[0]
        self.cimType = constructor['cimType']

    def set(self, value):
        if value in self.members:
            self.value = value
        else:
            raise ValueError(
                'Attempt to set value <%s> of enum <%s> outside membership' %
                (value, self.cimType))

    def __str__(self):
        return self.value

    def __eq__(self, other):
        if not isinstance(other, umlEnum): return 0
        if self.members == other.members and self.value == other.value: return 1
        return 0

    def __ne__(self, other):
        return not self == other

    @property
    def pyesdoc(self):
        """ Method to return a pyesdoc version for printing, storing etc """
        #pysesdoc only cares about my value ...
        return self.value

def gather_base_properties(constructor, cimset):
    """ Gather all properties (included inherited properties) for a class
    :param klass: A cim class constructor
    :param cimset: A complete set of cim class constructors
    :return: List of all properties for the given klass
    """
    assert 'base' in constructor,'No base class in constructor %s' % constructor
    r = constructor['properties']
    if constructor['base'] is not None:
        r+=gather_base_properties(cimset[package_split(constructor['base'])[1]],cimset)
    return r

def checkmetamodel(constructor, cimset=None):
    """ 
    Checks consistency of a class constructor with our metamodel
    :param constructor: A cim class definition constructor
    :param cimset: The complete set of possible cim targets for property target testing
    :return: True if OK
    """
    try:
        # Next two attributes are not in the basic upstream definition functions but are inserted
        # by the esdoc-nb/mp framework to support the downstream API.
        assert 'cimType' in constructor, 'Missing cimType'
        assert 'doc' in constructor, 'Missing doc'
        # The rest of these tests are the basic upstream metamodel understood by esdoc-mp
        assert 'is_abstract' in constructor,'Missing is_abstract'
        assert 'properties' in constructor, 'Missing properties'
        assert 'base' in constructor, 'Missing base'
        assert isinstance(constructor['properties'], list), 'Properties not a list'
        for p in constructor['properties']:
            for i in p:
                if i is None: print constructor,'\n',p
                assert i[0] != ' ','%s No blanks at start of property terms' % str(p)
            if cimset:
                assert package_split(p[1])[1] in cimset or p[1] in classmap.keys(), \
                    'property target failure for %s' % p[1]
        if 'constraints' in constructor:
            assert isinstance(constructor['constraints'], list), "Constraints not a list"
            for c in constructor['constraints']:
                 assert c[1] in ['value', 'from', 'hidden', 'include', 'cardinality'],\
                    'Constraint failure %s' % c[1]
                 if cimset:
                    targets = {}
                    available = gather_base_properties(constructor, cimset)
                    for a in available:
                        targets[a[0]] = a[1]
                    assert c[0] in targets,\
                        'Constraint on non-existent property %s (%s)' % (c[0], targets)
                    this_target = package_split(targets[c[0]])[1]
                    if c[1] == 'include':
                        # check the include classes are of the right type for the property target
                        assert isinstance(c[2],list),'Include constraint not a list %s' % c
                        for t in c[2]:
                            tp = package_split(fix_fully_qualified_name(t))[1]
                            assert tp in cimset,'Constraint include target not known %s %s' % (
                                tp, sorted(cimset.keys()))
                            if cimset[tp]['type'] == 'class':
                                instance = umlClass(cimset[tp], cimset)
                            else: instance = umlEnum(cimset[tp])
                            assert isCIMinstance(instance,this_target), \
                                '%s is not of type %s' % (tp, this_target)
        if 'alternatives' in constructor:
            # Can't rely on this if constructor supplied via factory ...
            assert isinstance(constructor['alternatives'],list), 'Alternatives not a list'
            if cimset:
                for p in constructor['alternatives']:
                    assert p in cimset, 'Non existent alternative class [%s]' % p

    except AssertionError,e:
        print constructor
        raise ValueError('%s fails metamodel test (%s)' % (constructor['cimType'], str(e)))
    except Exception, e:
        print constructor
        import traceback
        traceback.print_exc()
        raise Exception(e)
    return 1

def make_string(obj):
    """Make a string version of a umlClass object"""
    if hasattr(obj,'pstr'):
        printable = [getattr(obj, k) for k in obj.pstr[1]]
        # now see if any of those are lists
        revised = []
        for a in printable:
            if isinstance(a, list):
                if len(a) == 1:
                    revised.append(str(a[0]))
                else:
                    revised.append(', '.join(tuple([str(aa) for aa in a])))
            else:
                revised.append(a)
        output = tuple(revised)
        a = obj.pstr[0] % output
        return a
    elif hasattr(obj, 'name'):
        return '%s' % (obj.name,)
    else:
        return obj.cimType

def convert_from_pyesdoc(doc, target_type=None):
    """ Converts a pyesdoc class instance into a pseudo_mp umlClass instance
    :param doc: a document encoded as a pyesdoc class instance
    :param target_type: if known, the target type
    :return doc: a notebook pseudo_mp umlClass instance
    """
    # We normally know the target_type once we're dealing with the attributes
    # of a class_instance, it's only the "top" class instance we would parse.
    if not target_type:
        target_type = doc.type_key.split('.')[-1]
    target = make_cim_instance(target_type)
    # First check if this is an enum string value, if so, just set the value and leave
    if isinstance(doc, str):
        target.set(doc)
        return target
    # OK, now we know it's a class we're dealing with:
    for a in target.attributes:
        umlp = target.attributes[a]
        attr = umlp.name
        value = getattr(doc, attr)
        if value is None:
            setattr(target, attr, value)
        elif umlp.target not in classmap:
            if umlp.single_valued:
                setattr(target, attr, convert_from_pyesdoc(value, umlp.target))
            else:
                setattr(target, attr, [convert_from_pyesdoc(v, umlp.target) for v in value])
        else:
            setattr(target, attr, value)
    return target

def make_cim_instance(cimtype, factory=None):
    """ Make a cim instance,
    either using the default factory class (factory = None),
    or using whatever factory is currently in use (with the provided factory).
    In the latter case this will use extension points to change the class
    actually delivered, if appropriate """
    if factory is None:
        factory = cimFactory()
    if cimtype in factory.fullset:
        extended = False
        # If appropriate, build using the class at the extension point
        for p in extension_points:
            if p in factory.packages:
                if cimtype in extension_points[p]:
                    cimtype = extension_points[p][cimtype]
                    extended = True
        instance = factory.build(cimtype)
        return instance
    else:
        raise ValueError('Factory cannot instantiate unknown non abstract class/enum [%s][%s]' %
                         (cimtype, sorted(factory.fullset)) )

def makeCimInstanceFromSet(cimtype, cimset):
    """ Makes a cim instance of cimtype from within a set of possible cimtypes: cimset """
    cimtype = package_split(cimtype)[1]
    assert cimtype in cimset,'%s not in %s' % (cimtype,sorted(cimset.keys()))
    constructor = cimset[cimtype]
    assert 'type' in constructor
    assert constructor['type'] in ['class', 'enum']
    if constructor['type'] == 'class':
        return umlClass(constructor, cimset)
    elif constructor['type'] == 'enum':
        return umlEnum(constructor)

def makeQuickText(content):
    ''' Given some plain text, turn it into a cimtext object '''
    c = make_cim_instance('Cimtext')
    tc = make_cim_instance('TextCode')
    tc.set('plaintext')
    c.content_type = tc
    c.content = content
    return c

def isCIMinstance(instance, rtype):
    """ CIM aware version of python isinstance, ensures that cim instances
    respect the notional class inheritance. """
    if isinstance(instance,umlClass):
        class_tree =[instance.cimType,] + instance.base
        if rtype in class_tree:
            return 1
        else:
            return 0
    elif isinstance(instance, umlEnum):
        assert not hasattr(instance,'base'),"isCIMinstance doesn't support enums with base classes"
        return instance.cimType == rtype
    else:
        ok = max([isinstance(instance, r) for r in classmap[rtype]])
        return ok

class TestFactory(unittest.TestCase):
    def testFactory(self):
        f = cimFactory()
        for c in ['Cimtext',]:
            assert c in f.classes, 'Class Failure %s not in %s' % (c, sorted(f.classes.keys()))
        for e in ['GridTypes',]:
            assert e in f.enums, 'Enum Failure %s not in %s' % (e, sorted(f.enums.keys()))

    def testDateTime(self):
        """ Something odd happens when we build a CIM DateTime"""
        x = make_cim_instance('DateTime')
        self.assertEqual(len(x.attributes), 2)

    def test_apibase(self):
        """Tests the apifix on base classes"""
        f=cimFactory()
        dummy={'properties':[('abc','numerical_experiment','1.1',''),],'base':'activity'}
        self.assertEqual('Activity',f._apifix(dummy)['base'],
                         'Failed to convert base class to Notebook API')

    def test_getbase(self):
        """ test finding base classes"""
        f = cimFactory()
        b = f.base('NumericalExperiment')
        self.assertEqual(b, ['Activity'])

    def test_expandclasses(self):
        """ Test expanding classes and exploiting extension points"""
        f = cimFactory()
        constructors = cmip6.get_constructors()
        f.add_package_by_functions('cmip6', constructors)
        self.assertIn('Cmip6Model', f.fullset)
        m = make_cim_instance('Model', f)
        self.assertEqual('Cmip6Model', m.cimType)


class TestProperty(unittest.TestCase):

    def setUp(self):
        self.p = ('long_name', 'str', '0.1', 'Additional alternative long name')
        self.q = ('name', 'str', '1.1', 'Common name of machine')
        self.r = ('experiments', 'linked_to(activity.NumericalExperiment)', '1.N',
                  'Experiments required/requested within the project')
        self.umlp = umlProperty(self.p)
        self.umlq = umlProperty(self.q)
        self.umlr = umlProperty(self.r)

    def testReadonlyProperty(self):
        """ Test cim UML properties are readonly in property instances"""
        with self.assertRaises(AttributeError):
            self.ump.name = 'fred'

    def testBasicAPI(self):
        """Basic API test"""
        apiout=(self.umlp.name, self.umlp.target, self.umlp.cardinality, self.umlp.doc)
        self.assertEqual(apiout, self.p)

    def testSingleValued(self):
        """ Check single valued does what we think it should"""
        self.assertTrue(self.umlp.single_valued)
        self.assertTrue(self.umlq.single_valued)
        self.assertFalse(self.umlr.single_valued)

    def testSetter(self):
        """Test we can assign values"""
        name = 'ARCHER'
        self.umlq.set(name)
        self.assertEqual(self.umlq.value, name)

    def testValidSetting(self):
        """ Ensure we can't set the wrong types to properties """
        self.assertRaises(ValueError, self.umlp.set, 1.0)

    def testMultiValueSetting(self):
        """ Test we can't set a multivalued property"""
        e = make_cim_instance('NumericalExperiment')
        self.assertRaises(ValueError, self.umlr.set, e)

    def testlinkage(self):
        """ Test the islink property """
        self.assertTrue(self.umlr.islink)
        self.assertFalse(self.umlp.islink)

    def testReferenceStereotype(self):
        """ i.e that we see an online resource for linked_to objects
        rather than the actual type"""
        # FIXME put a test here
        pass

    def testStrProperty(self):
        """Ensure that a proper string version is possible"""
        repr(self.umlp)


class TestMethods(unittest.TestCase):
    def testIsDocument(self):
        """Simple test: these should be documents"""
        f = cimFactory()
        x = ['Party','NumericalExperiment','Simulation']
        for c in x:
            self.assertTrue(f.isdoc(c),' Failed with %s' % c)

    def testIsNotDocument(self):
        """Simple test: these should not be documents"""
        x = ['DateTime', 'Cimtext']
        f = cimFactory()
        for c in x:
            r = f.isdoc(c)
            self.assertFalse(r, '[%s] should not be reported as a document' % c)

    def testIsCIMInstance(self):
        """ Make sure we can assign a subclass to a thing which expects as superclass"""
        x = make_cim_instance('Party')
        y = make_cim_instance('MinimalMeta')
        x.meta = y
        z = make_cim_instance('Meta')
        self.assertTrue(isCIMinstance(z,'MinimalMeta'))
        x.meta = z

    def testInstanceID(self):
        """ Test expectation that things are cimClasses and cimEnums"""
        c = make_cim_instance('NumericalExperiment')
        assert isinstance(c,umlClass)
        e = make_cim_instance('TextCode')
        assert isinstance(e,umlEnum)


class TestMyClasses(unittest.TestCase):
    """ Build and test all classes can be instantiated """

    def __testmaker(self, t):
        """ Wrap around a make so we know what it is if it goes wrong """
        try:
            x = make_cim_instance(t)
        except:
            print 'Unable to make %s' % t
            x = make_cim_instance(t)
        for y in x.attributes:
            p = x.attributes[y]
            if p.target not in classmap:
                try:
                    z = make_cim_instance(p.target)
                except:
                    print y
                    print 'Failing with property %s in %s' % (p.target, t)
                    z = self.__testmaker(p.target)
                self.assertTrue(isCIMinstance(z, p.target), 'Failed with '+p.target)
        return x

    def testAllClasses(self):
        """Test instantiating all known CIM classes"""
        factory = cimFactory()
        for k in factory.classes:
            checkmetamodel(factory.classes[k],factory.fullset)
            x = self.__testmaker(k)
            self.assertTrue(isCIMinstance(x, k),'failed with %s' % k)

    def testCardinalityConstraint(self):
        """Test cardinality works"""
        p = make_cim_instance('Project')
        d = p.attributes['description']
        self.assertEqual('1.1', d.cardinality)

    def testValueConstraint(self):
        """Test setting fixed value works"""
        cimlink = make_cim_instance('CimLink')
        p = cimlink.attributes['description']
        self.assertTrue(p.isFixed)
        d = cimlink.description
        self.assertTrue(d.startswith('Link to'))

    def testHiddenConstraint(self):
        """ Test hiding a superclass property from a subclass """
        nr = make_cim_instance('NumericalRequirement')
        self.assertNotIn('duration', nr.attributes)

    def testValidate(self):
        """ Make sure that thing are marked invalid until
        all attributes are present"""
        m=make_cim_instance('Project')
        m.name = 'test'
        meta = make_cim_instance('Meta')
        m.meta = meta
        self.assertTrue(not m.validate(True))
        with self.assertRaises(ValueError):
            m.validate()
        m.description = makeQuickText('abc')
        self.assertTrue(m.validate())

    def testDeepcopy(self):
        """ What does deepcopy do with these classes?"""
        m = make_cim_instance('Meta')
        p = make_cim_instance('Project')
        p.meta = m
        p.name = 'name1'
        x = copy.deepcopy(p)
        self.assertEqual(x, p)
        p.name = 'name2'
        self.assertNotEqual(x, p)

    def testAlternativesOfSubClasses(self):
        ''' Test that self type is not in alternatives of subclasses '''
        stc = make_cim_instance('TemporalConstraint')
        self.assertEqual('TemporalConstraint' not in stc.alternatives, True)
        self.assertEqual('NumericalRequirement' in stc.alternatives, True)

    def testAlternatives(self):
        ''' Make sure all alternative classes are subclasses of parent '''
        factory = cimFactory()
        for c in factory.classes:
            d = factory.classes[c]
            if 'alternatives' in d:
                for a in d['alternatives']:
                    aa = make_cim_instance(a)
                    dd = make_cim_instance(d['cimType'])
                    ok = isCIMinstance(aa, dd.cimType)
                    self.assertTrue(ok, 'Failing on %s %s' % (a, d['cimType']))

    def testMetaLastUpdated(self):
        m=make_cim_instance('Meta')
        m.metadata_last_updated = datetime.now().isoformat(' ')



class TestEnums(unittest.TestCase):

    def testAllEnums(self):
        ''' Test instantiating all known CIM enums '''
        f = cimFactory()
        for k in f.enums:
            try:
                make_cim_instance(k)
            except:
                print 'Failing to make %s' % k
                make_cim_instance(k)

    def testEnumSetting(self):
        f = cimFactory()
        tp = f.build('TimeUnits')
        self.assertRaises(ValueError, tp.set, 'bnl')
        tp.set('days')
        self.assertEqual(str(tp), 'days')

    def test_cmip6_enums(self):
        f = cimFactory()
        f.add_package_by_functions('cmip6',cmip6.get_constructors())
        self.assertIn('SeaIcePossibleSnowProcesses',f.enums)

    def test_cmip6_class_and_enum(self):
        f = cimFactory()
        f.add_package_by_functions('cmip6',cmip6.get_constructors())
        x = f.build('SeaIceSnowProcesses')


class MiscTests(unittest.TestCase):

    def testStringFormat(self):
        """ Test simple pstr use case """
        tp = make_cim_instance('TimePeriod')
        u = make_cim_instance('TimeUnits')
        u.set('days')
        tp.length = 10
        tp.units = u
        self.assertEqual(str(tp)[0:7], '10 days')

    def testQuickText(self):
        m = make_cim_instance('Model')
        m.name = 'FastModel'
        m.description = makeQuickText('Dummy model')
        self.assertTrue(isCIMinstance(m.description, 'Cimtext'))


class SerialisationTests(unittest.TestCase):

    def setUp(self):
        """ Provide some re-usable instances for the tests"""
        f = cimFactory()
        m = make_cim_instance('MinimalMeta', f)
        a = make_cim_instance('Party', f)
        r = make_cim_instance('Responsibility', f)
        rc = make_cim_instance('RoleCode', f)
        rc.set('author')
        a.name = 'Author Test'
        a.meta = m
        r.party.append(a)
        r.role = rc
        self.author = r
        numexp = f.build('NumericalExperiment')
        numexp.name = 'An experiment'
        numexp.responsible_parties.append(self.author)
        self.numexp = numexp

    def test_pyesdoc_single(self):
        """ Test that one attribute in an mp class instance can be converted to pyesdoc"""
        x = self.numexp
        y = x.pyesdoc
        self.assertEqual(y.name, x.name)

    def test_twoway(self):
        """ Tests simple two way conversion for entire class instance"""
        x = self.numexp
        y = x.pyesdoc
        z = convert_from_pyesdoc(y)
        self.assertEqual(z, x)

    def test_pyesdoc_all(self):
        """ Test all classes can be converted to pyesdoc"""
        factory = cimFactory()
        for k in factory.classes:
            x = factory.build(k)
            y = x.pyesdoc
            z = convert_from_pyesdoc(y)
            self.assertEqual(z,x,'Cannot two way convert class %s' % k)


class PackageTest(unittest.TestCase):
    """ Test that all properties exist in the correct package"""
    def test_packages(self):
        """ Check properties"""
        constructors, packages = get_from_modules(class_packages)
        enum_constructors, e_packages = get_from_modules(enum_packages)
        results = []
        for pkg in packages:
            # looping over all constructors in the package
            for k in packages[pkg]:
                constructor = constructors[k]
                # only need to loop over classes not enums
                if constructor['type'] == 'class':
                    for p in constructor['properties']:
                        pfixed = fix_all_names(p[1])
                        target_pkg, target_klass = package_split(pfixed)
                        if target_pkg is None:
                            if target_klass in packages[pkg]:
                                pass
                            elif pkg in e_packages and target_klass in e_packages[pkg]:
                                pass
                            elif target_klass in classmap:
                                pass
                            else:
                                results.append('%s, %s, %s, %s' % (pkg, k, target_pkg, target_klass))
                        else:
                            if target_pkg in e_packages:
                                if target_klass in e_packages[target_pkg]:
                                    pass
                            elif target_klass not in packages[target_pkg]:
                                results.append ('%s, %s, %s, %s, %s, %s' % (pkg, k, p[0],
                                            target_pkg, target_klass, packages[target_pkg]))





if __name__ == "__main__":
    unittest.main()


