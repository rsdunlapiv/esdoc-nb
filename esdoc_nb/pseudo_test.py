import unittest
import tempfile

import esdoc_nb.mp.pseudo_mp as mp
import pseudo_storage as ps


def makeTestData():
    ''' Makes test data for unit tests '''
    tdir=tempfile.mkdtemp()
    store=ps.cimStorage(tdir)
    for i in range(5):
        e=mp.make_cim_instance('numericalExperiment')
        m=mp.make_cim_instance('meta')
        r=mp.make_cim_instance('party')
        d=mp.make_cim_instance('cimtext')
        r.name='Tester %s'%i
        m.append('metadata_author',r)
        e.meta=m
        e.name='Experiment %s'%i
        re=range(5)
        del re[i]
        for j in re:
            cl=mp.make_cim_instance('cimLink')
            cl.name='Experiment %s'%j
            cl.remoteType='numericalExperiment'
            e.append('related_to_experiment',cl)
            e.description=mp.makeQuickText('A lot of lovely content')
        store.add(e)
    return store
    
class TestmakingTestData(unittest.TestCase):
    ''' Tests the dummy data itself '''
    
    def testDummy(self):
        store=makeTestData()
        self.assertEqual(len(store.Experiments),5)
    
    def testExpNames(self):
        ''' test the names are what we think they are '''
        # so that other routines can use those names
        store=makeTestData()
        for k in store.Experiments:
            e=store.Experiments[k]
            self.assertEqual(str(e),e.name)
        
        
if __name__=="__main__":
    unittest.main()
        
        
