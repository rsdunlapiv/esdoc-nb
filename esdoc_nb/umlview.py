
# inspired by django extensions ... 

import unittest
import os, re, math

import pygraphviz as pgv

import esdoc_nb.mp.pseudo_mp as mp

class PackageColour(object):
    """ Set of pastel colours and methods for colourising
    package classes.
    """
    def __init__(self, packages):
        """
        :param packages: list of packages to colour
        """
        self.palette2 = {
            'pearl': (243, 239, 237),
            'pink': (242, 225, 233),
            'pale_brown': (242, 218, 209),
            'beige': (243, 227, 201),
            'sea_blue': (180, 221, 225),
            'green': (213, 225, 177),
            'brown': (212, 205, 200),
            'sky_blue': (206, 218, 241),
            'grey': (193, 195, 205),
            'orange': (245, 208, 144),
            'purple':(2225,175, 200,)
        }
        self.palette = ['wheat', 'sandybrown', 'darkkhaki', 'aliceblue', 'seashell', 'plum1',
                       'lightgoldenrodyellow', 'lightgrey', 'rosybrown', 'lightcyan']
        assert len(packages) <= len(self.palette), "Too many packages (%s) to colour" % len(packages)
        self.packages = packages
        self.colours = {}
        for i, p in enumerate(packages):
            self.colours[p] = self.palette[i]

    def colourise(self,klass):
        """ Given a klass return the appropriate package colour"""
        for p in self.colours:
            if klass in self.packages[p]:
                break
        return self.colours[p]


class Palette(object):
    """ Used to hold label colour palette """
    orig = (105, 139, 34), (238, 232, 170),   # olivedrab4, palegoldenrod
    brown = (139, 125, 107),  (238, 232, 170),  # bisque3, palegoldenrod
    purple = (162, 142, 188), (224, 216, 232)
    choices = ['abs','doc','def']

    def __htmlhex(self,rgb):
        return '#'+"".join(map(chr, rgb)).encode('hex')

    def top(self, choice):
        """ Colour for top of choice """
        assert choice in self.choices
        return self.__htmlhex({'abs': self.purple[0],
                'doc': self.orig[0],
                'def': self.brown[0]}[choice])

    def main(self, choice):
        """ Colour for background of choice"""
        assert choice in self.choices
        return self.__htmlhex({'abs': self.purple[1],
                'doc': self.orig[1],
                'def': self.brown[1]}[choice])


def get_class_label(model, show_base=True, exclude_assocs=[], is_document=False):

    """Gets an html table string for a particular cim class model.
    If show_base, show any base classes, otherwise exclude
    them (we assume the base class relation is shown explicitly.
    If exclude_assocs is present, it's a list of
    classes which are also being shown, and so they will be
    shown by an explicit association link."""

    def row(content, align="left", border=None, color=None, sides=None):
        """ Convenience method to wrap std html row """
        b, c, s = '','',''
        if border is not None:
            b = 'BORDER="%s"' % border
        if color is not None:
            c = 'BGCOLOR="%s"' % color
        if sides is not None:
            s = 'SIDES="%s"' % sides
        s = '<TR><TD ALIGN="%s" CELLPADDING="2" %s %s %s>%s</TD></TR>' % (
            align, b, c, s, content)
        return s

    def border_first(thelist, align="left"):
        """Convenience method to produce html rows, but make the
        first one have a line on top """
        if len(thelist) == 0:
            return ''
        s = row(thelist[0], border=1, sides='T', align=align)
        for r in thelist[1:]:
            s += row(r)
        return s

    palette = Palette()
    if model['is_abstract']:
        colour_choice = 'abs'
    elif is_document:
        colour_choice = 'doc'
    else:
        colour_choice = 'def'
    bk_col = palette.main(colour_choice)
    hdr_col = palette.top(colour_choice)

    s = '<<TABLE BGCOLOR="%s" BORDER="1" CELLBORDER="0" CELLSPACING="0">' % bk_col
    if model['base'] and show_base:
        s += '<TR><TD ALIGN="right" BGCOLOR="%s">' % hdr_col
        s += '<FONT FACE="Helvetica Italic" COLOR="white">&lt;%s&gt;</FONT></TD></TR>' % model['base']
    s += '<TR><TD ALIGN="CENTER" BGCOLOR="%s">' % hdr_col
    try:
        if model['is_abstract']:
            s += '<FONT FACE="Helvetica Bold" COLOR="white">&lt;&lt;abstract&gt;&gt;</FONT></TD></TR>'
            s += '<TR><TD ALIGN="CENTER" BORDER="1" SIDES="T" BGCOLOR="%s">' % hdr_col
            name = '<i>%s</i>' % model['cimType']
        else:
            name = model['cimType']
    except:
        raise ValueError('Broke with %s' % model['cimType'])
    s += '<FONT FACE="Helvetica Bold" COLOR="white">%s</FONT></TD></TR>' % name
    shown = 0
    for p in model['properties']:
        if p[1] not in exclude_assocs:
            up = mp.umlProperty(p)
            shown = 1
            s += row(' +%s: %s &#91;%s&#93;' % (up.name, up.fulltarget, up.cardinality))
    if 'derived' in model:
        the_list = [' +%s()' % m[0] for m in model['derived']]
        s += border_first(the_list)
    if 'constraints' in model:
        the_list = []
        for c in model['constraints']:
            c = list(c)
            if c[1] == 'include':
                the_list.append(' %s: {%s' % (c[0],c[1]))
                for cc in c[2][0:-1]:
                    fmt1 = '      %s'
                    fmt2 = '      %s}'
                    the_list.append(fmt1 % cc)
                the_list.append(fmt2 % c[2][-1])
            else:
                the_list.append(' %s: {%s}' % (c[0], '='.join(c[1:])))
        s += border_first(the_list)
    if not shown:
        s += '<tr><td cellpadding="2">&nbsp;</td></tr>'
    s += '</TABLE>>'
    return s


def get_enum_label(model):
    """ Return an enum label """
    palette = Palette()
    s = '<<TABLE BGCOLOR="%s" BORDER="1" CELLBORDER="0" CELLSPACING="0">' % palette.main('def')
    s += '<TR><TD ALIGN="CENTER" BGCOLOR="%s">' % palette.top('def')
    s += '<FONT FACE="Helvetica Bold" COLOR="white">%s</FONT></TD></TR>' % model['cimType']
    for p in model['members']:
        s += '<TR><TD ALIGN="left" CELLPADDING="2"'
        s += ' BORDER="0"> %s </TD></TR>' % p[0]
    s += '</TABLE>>'
    return s


class umlDiagram(object):
    """ Represents a UML diagram and various method on producing it """
    
    default_node_attributes = {'shape': 'plaintext', 'fontsize': '10'}
    
    def __init__(self):
        ''' Initialise,arguments tbd '''
        self.classes2view = []
        self.associations = []
        self.invisible_edges = []
        self.package = None
        self.factory = mp.cimFactory()

    def __expand(self, c, get_base=True):
        ''' For a given class, find all relevant classes '''
        return self.factory.find_associates(c, get_base=get_base)
        
    def __base(self, c):
        ''' For a given class, find base classes '''
        return self.factory.base(c)
        
    def __packageGraphDefaults(self,G):
        ''' Some defaults for the various packages when only
        showing classes (no assocations)... '''
        defaults={'activity':[('rankdir','LR'),],
                  'platform':[('rankdir','LR'),]}
        if self.package in defaults:
            for k,v in defaults[self.package]:
                G.graph_attr[k]=v



        
    def plot(self,layout='dot',fmt='png',filebase=None, title=None, 
                    makedot=False, dpi=150, nwidth=0, name_only=False, **kw):
        """Make a plot once everything is set up. If name_only, then
        don't use the nice UML labels, just do a traditional dot
        file and make sure that only nodes that appear in allup appear
        (i.e. ignore node and edges that bring in base classes outside
        that list). """

        def __shorten(c):
            """ Convenience method for narrower width labels
            :param c: A camel case name
            :return: Camels with line splits
            """
            words = re.findall('[A-Z][^A-Z]*',c)
            return '\n'.join(words)

        def __add_ranks(nodes, width=4):
            """ Take as set of nodes which would otherwise have the same row rank
            on the graph and add some invisible edges to make them flow down the
            page rather than across the page """
            stride = int(math.ceil(len(nodes)/float(width)))
            rank = nodes[0:width]
            for row in range(1, stride):
                si = row * width
                ei = min(si + width, len(nodes))
                under = nodes[si:ei]
                for i in range(len(under)):
                    G.add_edge(rank[i], under[i], style='invis')
                rank = under

        G = pgv.AGraph(directed=True, strict=False, dpi=dpi, **kw)
        G.graph_attr['splines'] = True
        G.graph_attr['fontsize'] = '8'
        if self.associations == []:
            # slightly helps we get tall instead of wide ...
            # but realistically at some point we should
            # handle layout more explicitly.
            if self.package:
                self.__packageGraphDefaults(G)
        G.graph_attr['page'] = '8.5,11'
        if title is not None:
            G.graph_attr['label'] = title

        picker = PackageColour(self.factory.packages)
        # get all the classes to show
        for c in self.allup:
            assert c in self.factory.classes or c in self.factory.enums,'Unexpected klass %s' % c
            if c in self.factory.classes:
                m = self.factory.classes[c]
                m_isdoc = self.factory.isdoc(c)
                label = get_class_label(m, show_base=self.baseControl[c],
                                    exclude_assocs=self.associations,
                                    is_document=m_isdoc)
            elif c in self.factory.enums:
                m = self.factory.enums[c]
                label = get_enum_label(m)

            if name_only:
                if c in self.allup:
                    label = __shorten(c)
                    G.add_node(c, label=label, fillcolor=picker.colourise(c), style='filled')
            else:
                G.add_node(c)
                node = G.get_node(c)
                node.attr['label'] = str(label)
                for a in self.default_node_attributes:
                    node.attr[a] = self.default_node_attributes[a]
        
        # now do all the class edges,
        for e, f in self.class_edges:
            if e in self.allup and f in self.allup:
                G.add_edge(e, f, dir='back', arrowtail='empty')

        # now the invisible edges, if any:
        for e,f in self.invisible_edges:
            G.add_edge(e, f, style='invis')
            
        if self.associations:
            # dealing with: c, up.target, up.name, up.cardinality
            eindex=0
            for e, f, g, m in self.assoc_edges:
                eindex += 1
                # add extra spacing to avoid labels overlapping lines
                edge_label = ' %s ' % g
                edge_head_label = '  %s   ' % m
                G.add_edge(e, f,'named%s'%str(eindex),label=edge_label, headlabel=edge_head_label)
                #edge_head_label = ' %s\n%s' % (m, g)
                #G.add_edge(e, f, 'named%s'%str(eindex), headlabel=edge_head_label)


        # handle layouts drifting too wide
        if nwidth != 0:
            singletons = []
            for n in G.nodes():
                successors = G.successors(n)
                predecessors = G.predecessors(n)
                if len(predecessors)<=1 and len(successors)>nwidth:
                    fix_edges = []
                    for nn in successors:
                        ns = G.successors(nn)
                        if ns == 0:
                            fix_edges.append(nn)
                    __add_ranks(successors, nwidth)
                elif len(predecessors) == 0 and len(successors) == 0:
                    singletons.append(n)

            print singletons
            if len(singletons) > nwidth:
                __add_ranks(singletons, nwidth)

        G.layout(layout)
        if filebase is None:
            filebase = self.classes2view[0]
        graphout = '%s.%s' % (filebase, fmt)
        dotout = '%s.dot' % filebase
        G.draw(graphout, fmt)
        self.output_files = [graphout, ]
        if makedot: 
            G.write(dotout)
            self.output_files.append(dotout)
        self.G = G
        
    def setClasses(self, classes2view, show_base_classes=True):
        """ Set default content given initial classes """
        self.classes2view = classes2view
        allup = [c for c in self.classes2view]
        for c in self.classes2view:
            e = self.__expand(c, get_base=show_base_classes)
            for i in e:
                if i not in allup: allup.append(i)
        self.allup = allup
        self.__findClassEdges()
        
    def __findClassEdges(self):
        """Now parse the classes we've got, and if they have
        baseclasses in the set to be shown, collect the edges
        and remove the baseclass decoration."""
        self.class_edges = []
        self.baseControl = {}
        for c in self.allup:
            b = self.factory.base(c, False)
            if b in self.classes2view:
                # we should have the baseclasses to link to
                # otherwise it doesn't matter
                self.baseControl[c] = False
                if b <> []:
                    self.class_edges.append((b[0], c))
            else:
                # we are looking at the derivative classes
                if b == []:
                    self.baseControl[c] = False
                else:
                    b = b[0]
                    if b in self.allup:
                        self.baseControl[c] = False
                        self.class_edges.append((b, c))
                    else:
                        self.baseControl[c] = True
                        
        # now we have to do something about ranking 
        # otherwise we're all at sea.
        # so at this point, we'll get information for grapher
        # first parse, get all the top classes
        topset={}
        for e,f in self.class_edges:
            if f in topset: del topset[f]
            if e not in topset: topset[e]={'n':0,'u':0}
        # second parse, count number of children
        for e,f in self.class_edges:
            if e in topset: topset[e]['n']+=1
        self.topset=topset
        print self.baseControl
    
    def setAssociationEdges(self,docs=True,all=False):
        """Find the associations internal to a set of classes
        and pull them out of the labels"""
        self.associations = []
        self.assoc_edges = []
        for c in self.allup:
            if c in self.factory.classes:
                defn = self.factory.classes[c]
                for p in defn['properties']:
                    up = mp.umlProperty(p)
                    if up.target in self.allup:
                        self.associations.append(p[1])
                        self.assoc_edges.append(
                        (c, up.target, up.name, up.cardinality))
        
    def setPackage(self, package, externals=False, no_enums=False, no_classes=False):
        """Set initial via package definition. If externals, add any classes
         which are base classes for classes in this package."""
        assert not (no_classes and no_enums), "set package choices mean nothing to plot"
        self.package = package
        self.classes2view = self.factory.packages[package]
        if no_enums:
            for p in self.classes2view:
                if p in self.factory.enums:
                    self.classes2view.remove(p)
        if no_classes:
            for p in self.classes2view:
                if p in self.factory.classes:
                    self.classes2view.remove(p)

        if externals:
            for c in self.classes2view:
                b = self.factory.base(c, follow=False)
                assert len(b) <= 1
                if b:
                    if b[0] not in self.classes2view:
                        self.classes2view.append(b[0])
        self.allup = self.classes2view
        self.__findClassEdges()

    def direct_layout(self, relationships):
        """ Adds direct control over the layout by providing a set of invisible edge relationships
        that can be used to provide additional control over layout. Primarily used for manually adjusting
        layouts that are too wide. Should be called after, for example setPackage.
        :param relationships: A list of tuples of the form [(higherclass, lowerclass), (lowerclass, bottomclass) ...]
        """
        self.invisible_edges = relationships

        
    def setClassDoc(self,class_name, get_base=True):
        """ Create an image for the documentation of a specific class.
        Should include all enums and the class itself, with all
        super-classes as well, but not the non-enum properties."""
        self.classes2view = [class_name, ]
        self.allup = [class_name, ]
        e = self.__expand(class_name, get_base=get_base)
        # now reject all the association properties and keep only the
        # enums and base classes
        for c in e:
            if c in self.factory.enums: self.allup.append(c)
        # now put back the base classes
        self.allup += self.factory.base(class_name)
        self.__findClassEdges()

    def setPerspective(self, class_name):
        """ Find all the classes which are one hop from class_name
        and set them as the wanted classes for a diagram
        :param class_name: str, a CIM2 class name to be the central focus
        """
        wanted = [class_name,]
        defn = self.factory.classes[class_name]
        for p in defn['properties']:
            up = mp.umlProperty(p)
            if up.target in self.factory.classes:
                wanted.append(up.target)
            elif up.target in self.factory.enums:
                wanted.append(up.target)
        self.setClasses(wanted)
        
    def cleanup(self):
        """ Remove any output files created """
        for f in self.output_files: os.remove(f)


class PackageDiagram(object):
    """ Provides a complete package diagram """
    def __init__(self, packages=None):
        """  Instantiate a package diagram
        :param packages: A list of packages to display, if not included, all packages are displayed
        """
        self.factory = mp.cimFactory()
        if packages:
            self.packages = packages
        else:
            self.packages = self.factory.packages

    def p_label(self, name, rows):

        def im(rr):
            """ return class or enum images as appropriate"""
            if rr in self.factory.classes:
                return '><img scale="True" src="class.png"/></td>'
            else:
                return '><img scale="True" src="enum.png"/></td>'

        def font(t):
            """ Determine if document and if so, make bold """
            if self.factory.isdoc(t):
                return '<font point-size="14" color="blue">%s</font>' % t
            else:
                return '<font point-size="14">%s</font>' % t

        html = '<<table border="0" cellspacing="0" cellpadding="0" cellborder="1">' + \
                '<tr><td colspan="2" sides="ltr"><font point-size="16">%s</font></td>' % name + \
                '<td style="invis"></td></tr>'
        html += '<tr><td sides="lt" %s <td colspan="2" sides="tr" align="left">%s</td></tr>\n' % (im(rows[0]), font(rows[0]))
        for r in rows[1:-1]:
            html += '<tr><td sides="l" %s <td colspan="2" sides="r" align="left">%s</td></tr>\n' % (im(r), font(r))
        html += '<tr><td sides="lb" %s<td colspan="2" sides="br" align="left">%s</td></tr>\n' % (im(rows[-1]), font(rows[-1]))
        html += '</table>>'
        return html


    def plot(self, layout='dot', fmt='png', filebase=None, title=None, makedot=False, dpi=150, **kw):
        """ Actually plot the diagram
        :param fmt: The output format
        :param filebase: Base for package filename
        :param title:  Title to put on package diagram
        :param makedot: If true, write the dot file as well
        :param dpi: DPI
        :param kw: Any other keywords
        :return:
        """

        G = pgv.AGraph(directed=True, strict=False, dpi=dpi, **kw)
        G.graph_attr['splines'] = True
        G.graph_attr['fontsize'] = '10'
        if title:
            G.graph_attr['label'] = title

        for p in self.packages:
            rows = self.packages[p]
            label = str(self.p_label(p, rows))
            G.add_node(p, shape="plaintext", label=label)


        invisible=[('designing','shared_time'),('activity','shared'),('software','drs'),
                   ('science','platform'),('platform','data')]

        for j,k in invisible:
            G.add_edge(j, k, style='invis')

        G.add_subgraph(['designing','activity','software','science'], rank='min')
        G.add_subgraph(['shared','shared_time','platform','drs'], rank='same')
        G.add_subgraph(['data'], rank='sink')


        # final layout and plotting
        G.layout(layout)
        if filebase is None:
            filebase = 'packages'
        graphout = '%s.%s' % (filebase, fmt)
        dotout = '%s.dot' % filebase
        G.draw(graphout, fmt)
        self.output_files = [graphout, ]
        if makedot:
            G.write(dotout)
            self.output_files.append(dotout)

    def cleanup(self):
        """ Remove any output files created """
        for f in self.output_files:
            os.remove(f)







        
class TestGraphCases(unittest.TestCase):
    
    def test_makediagrams(self):
        ''' Simply makes activity diagrams '''
        d = umlDiagram()
        d.setClasses(d.factory.packages['activity'])
        d.plot(filebase='activity_plus')
        d.cleanup()
        
    def test_make_packages(self):
        d = umlDiagram()
        for p in d.factory.packages:
            d.setPackage(p)
            d.plot(filebase=p, title='cim2 %s package'%p)
            d.cleanup()

    def test_associations(self):
        d = umlDiagram()
        package = 'science'
        d.setPackage(package)
        d.setAssociationEdges()
        d.plot(filebase='test%s_assoc' % package)
        #d.cleanup()

    def test_makedocs(self):
        '''' test making docs can execute '''
        d = umlDiagram()
        for k in d.factory.classes:
            d.setClassDoc(k)
            d.plot(filebase='mp/docs/%s' % k)

            
    def test_filebaes(self):
        ''' test we can use a temporary file '''
        import tempfile
        fn,pfile=tempfile.mkstemp()
        d=umlDiagram()
        d.setClassDoc('NumericalExperiment')
        d.plot(filebase=pfile)
        ff=pfile+'.png'
        d.cleanup()

    def test_constraints(self):
        d = umlDiagram()
        d.setClasses(['SimulationPlan',])
        d.plot(filebase='constraints')
        d.cleanup()

    def test_package(self):
        d = PackageDiagram()
        d.plot(filebase='mp/docs/packages', layout='dot', fmt='pdf')

if __name__ == "__main__":
    unittest.main()
    
    
    

