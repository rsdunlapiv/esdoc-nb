
import os
import sys

from setuptools import setup, find_packages
from esdoc_nb import __version__

here = os.path.dirname(__file__)

# Since README.md is in Markdown using this as the long_description isn't ideal
# However, it is better than the two getting out of sync.
long_description = open(os.path.join(here, 'README.md')).read()

try:
    import pygtk
except ImportError:
    raise SystemExit('esdoc-nb requires pygtk which must be installed independently')


setup(name='esdoc-nb',
      version=__version__,
      description='A notebook tool for editing ESDOC CIM documents',
      long_description=long_description,
      author='Bryan Lawrence',
      author_email='Bryan.Lawrence@ncas.ac.uk',
      url='https://bitbucket.org/bnlawrence/esdoc-nb/',
      license='MIT',
      packages=find_packages(),
      #!NOTE: pygtk is also required but doesn't support pip install
      install_requires=['pygraphviz', 'pygtk'],
      entry_points = {
        'console_scripts': ['esdoc-nb = esdoc_nb.pseudo_cimNB:main'],
        },
      )

